-- DROP
DROP TABLE IF EXISTS `User_Meal`;
DROP TABLE IF EXISTS `User_Preference`;
DROP TABLE IF EXISTS `Meal_Food`;
DROP TABLE IF EXISTS `Meal_Preference`;

DROP TABLE IF EXISTS `User`;
DROP TABLE IF EXISTS `Meal`;
DROP TABLE IF EXISTS `Preference`;
DROP TABLE IF EXISTS `Food_NL`;

DROP PROCEDURE IF EXISTS `getMeals`;
DROP PROCEDURE IF EXISTS `getNextMeals`;
DROP PROCEDURE IF EXISTS `getMealIngredients`;

-- TABLES
CREATE TABLE IF NOT EXISTS `User` (
	id            	INT(10) AUTO_INCREMENT,
    email 			VARCHAR(50) NOT NULL,
    password 		VARCHAR(50) NOT NULL,
    goal 			VARCHAR(1),
    gender 			VARCHAR(1),
    activity		DOUBLE(10,3),
    allergies 		VARCHAR(200),
    age 			INT(10),
    weight 			INT(10),
    height 			INT(10),
    meals_a_day 	INT(10),
    need 			INT (10),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `Meal`(
	id            	INT(10) AUTO_INCREMENT,
	name	 	  	VARCHAR(255),
	time      		VARCHAR(20),
	description 	VARCHAR(2040),
	tip           	VARCHAR(255),
	photo_url      	VARCHAR(2040),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `Preference` (
	id 				INT(10) AUTO_INCREMENT,
	category 		VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `Food_NL` (
	id 						INT(10) AUTO_INCREMENT,
    product_groep			VARCHAR(255),
    product_groep_code 		INT,
    product_code 			INT,
    product_omschrijving 	VARCHAR(255),
    product_description 	VARCHAR(255),
    kcal		 			INT,
    kj		 				INT,
    eiwit 					DOUBLE,
    vet 					DOUBLE,
    verzadige_vetten 		DOUBLE,
    e_o_vetten 				DOUBLE,
    m_o_vetten 				DOUBLE,
    cholesterol 			DOUBLE,
    koolhydraten 			DOUBLE,
    suikers 				DOUBLE,
    vezels 					DOUBLE,
    water 					DOUBLE,
    calcium 				DOUBLE,
    fosfor 					DOUBLE,
    ijzer 					DOUBLE,
    natrium 				DOUBLE,
    kalium 					DOUBLE,
    magnesium 				DOUBLE,
    zink 					DOUBLE,
    koper 					DOUBLE,
    v_b1 					DOUBLE,
    v_b2 					DOUBLE,
    v_b6 					DOUBLE,
    v_b12 					DOUBLE,
    v_d 					DOUBLE,
    v_e 					DOUBLE,
    v_c 					DOUBLE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS `User_Meal` (
	user_id 		INT(10),
	meal_id 		INT(10),
	FOREIGN KEY (user_id) REFERENCES `User`(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (meal_id) REFERENCES `Meal`(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS `User_Preference` (
	user_id 		INT(10),
	preference_id 	INT(10),
	plus_count 		INT(10),
	min_count 		INT(10),
	FOREIGN KEY (user_id) REFERENCES `User`(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (preference_id) REFERENCES `Preference`(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS `Meal_Food`(
	meal_id 					INT(10),
	food_id						INT(10),
	percentage_of_food_in_meal 	DOUBLE,
	FOREIGN KEY (meal_id) REFERENCES `Meal`(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (food_id) REFERENCES `Food_NL`(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS `Meal_Preference` (
	meal_id 				INT(10),
	preference_id			INT(10),
	FOREIGN KEY (meal_id) REFERENCES `Meal`(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (preference_id) REFERENCES `Preference`(id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- PROCEDURES
DELIMITER $$
CREATE PROCEDURE `getMeals` (
	IN first	VARCHAR(20),
	IN second	VARCHAR(20),
	IN third	VARCHAR(20),
    IN fourth	VARCHAR(20),
	IN fifth	VARCHAR(20),
    IN sixth	VARCHAR(20),
    IN seventh	VARCHAR(20),
    IN eighth	VARCHAR(20)
)
BEGIN
	SET
		@first := first,
		@second := second,
		@third := third,
		@fourth := fourth,
		@fifth := fifth,
		@sixth := sixth,
		@seventh := seventh,
		@eighth := eighth,
		@time := null,
		@count := null;
	SELECT 
		m.*, 
		ROUND(SUM((f.kj * mf.percentage_of_food_in_meal) / 100), 2) AS kj,  
		ROUND(SUM((f.kcal * mf.percentage_of_food_in_meal) / 100), 2) AS kcal,
		ROUND(SUM((f.eiwit * mf.percentage_of_food_in_meal) / 100), 2) AS protein,
		ROUND(SUM((f.koolhydraten * mf.percentage_of_food_in_meal) / 100), 2) AS carbohydrates,
		ROUND(SUM((f.vet * mf.percentage_of_food_in_meal) / 100), 2) AS fat,
		@count:=IF(@time = time, @count + 1, 1) AS count,
		@time:= time AS time
	FROM meal m, meal_food mf, food_nl f
	WHERE m.id = mf.meal_id
	AND f.id = mf.food_id
	AND m.time IN (@first, @second, @third, @fourth, @fifth, @sixth, @seventh, @eighth)
	GROUP BY m.id
	HAVING count <= 3;
END $$

CREATE PROCEDURE `getNextMeals`(
	IN time 	VARCHAR(20), 
    IN lastId	INT,
    IN amount 	INT
)
BEGIN
	SET 
		@time = time,
		@lastId = lastId,
        @amount = amount,
		@query = 
        "SELECT 
			m.*, 
			ROUND (SUM((f.kj * mf.percentage_of_food_in_meal) / 100), 2) AS kj,  
			ROUND (SUM((f.kcal * mf.percentage_of_food_in_meal) / 100), 2) AS kcal,
			ROUND (SUM((f.eiwit * mf.percentage_of_food_in_meal) / 100), 2) AS protein,
			ROUND (SUM((f.koolhydraten * mf.percentage_of_food_in_meal) / 100), 2) AS carbohydrates,
			ROUND (SUM((f.vet * mf.percentage_of_food_in_meal) / 100), 2) AS fat
		FROM meal m, meal_food mf, food_nl f
		WHERE m.id = mf.meal_id
		AND f.id = mf.food_id
		AND m.time = ?
        AND m.id > ?
		GROUP BY m.id
		LIMIT ?";
    PREPARE statement FROM @query; 
	EXECUTE statement USING @time, @lastId, @amount;
	DEALLOCATE PREPARE statement;
END $$

CREATE PROCEDURE `getMealIngredients`(
    IN MealId	INT
)
BEGIN
	SET 
		@mealId = MealId,
		@query =        
        "SELECT f.*
		FROM meal m, meal_food mf, food_nl f
		WHERE m.id = mf.meal_id
		AND f.id = mf.food_id
		AND m.id = ?";
        
    PREPARE statement FROM @query; 
	EXECUTE statement USING @mealId;
	DEALLOCATE PREPARE statement;
END $$
DELIMITER ;