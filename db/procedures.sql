/* 
Input: profiel (caloriebehoefte, accountid)
Output: lijst van voedsel (sortMeal)
- 5 meal records van een soort.
- calorieen per meal berekenen en de meals in die hoeveelheden returnen.
- kijken waarvan de gebruiker niet houdt

*/

DROP PROCEDURE `getSomeMeals`;

DELIMITER $$
CREATE PROCEDURE `getSomeMeals`(IN sortMeal VARCHAR(20), IN need VARCHAR(20)/*, OUT po_ErrMessage VARCHAR(200)*/)
BEGIN

/*DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
SET po_ErrMessage = 'Error in procedure getSomeMeals';
END;*/


SELECT Meal.NameMeal, Meal.SortMeal, Meal.id, SUM(Food_NL.kcal) as kcal
FROM Meal, Meal_Food, Food_NL
WHERE Meal.id = Meal_Food.Meal_id
AND Food_NL.id = Meal_Food.Food_id
AND Meal.SortMeal = sortMeal
GROUP BY Meal.id
HAVING SUM(Food_NL.kcal) BETWEEN (need - 300) AND (need + 300);
END

/*met preferences*/
/*SELECT * FROM meal AS m1 JOIN(SELECT CEIL(RAND() * (SELECT MAX(id) - 5 FROM meal WHERE SortMeal = 'Ontbijt' AND ID IN (SELECT User_ID FROM user_preference WHERE User_ID = 1 AND Min_count <= Plus_count))) AS id) AS m2 WHERE m1.id >= m2.id AND m1.SortMeal = 'Ontbijt' ORDER BY m1.id ASC LIMIT 5;*/
