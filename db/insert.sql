-- Food data
LOAD DATA LOCAL INFILE 'Users/jimvercoelen/bitbucket/io3_repo/db/food_table.csv' 
INTO TABLE `Food_NL`
FIELDS TERMINATED BY ';' 
LINES TERMINATED BY '\n'(
	@Product_groep_oms,	
    @Product_groep_code,	
    @Product_code,	
    @Product_omschrijving,
    @Product_description,	
    @kcal,
    @kJ,
    @Eiwit,
    @vet,
    @verzadigd,
    @enkel_onverzadigd,	
    @meer_onverzadigd,
    @Cholesterol,                   	
    @Koolhydraten,
    @Suiker,
    @Voedingsvezel, 	
    @Water,                        	
    @Calcium,                         	
    @Fosfor,                          	
    @IJzer,
    @Natrium,                         	
    @Kalium,                      	
    @Magnesium,
    @Zink,                           	
    @Koper,                           	
    @V_B1,	
    @V_B2,	
    @V_B6,
    @V_B12,
    @V_D,
    @V_E,
    @V_C
) set 
	Product_groep = 		@Product_groep_oms,
    Product_groep_code = 	@Product_groep_code,
    Product_code =			@Product_code,
    Product_omschrijving = 	@Product_omschrijving,
    Product_description = 	@Product_description,
    kcal =					@kcal,
    kj =					@kj,
    Eiwit =					@Eiwit,
    Vet = 					@vet,
    Verzadige_vetten = 		@verzadigd,
    E_o_vetten =			@enkel_onverzadigd,
    M_o_vetten =			@meer_onverzadigd,
    Cholesterol =			@Cholesterol,
    Koolhydraten = 			@Koolhydraten,
    Suikers = 				@Suiker,
    Vezels =				@Voedingsvezel,
    Water =					@Water,
    Calcium =				@Calcium,
    Fosfor =				@Fosfor,
    Ijzer =					@Ijzer,
    Natrium = 				@Natrium,
    Kalium =				@Kalium,
    Magnesium =				@Magnesium,
    Zink = 					@Zink,
    Koper =					@Koper,
    V_B1 =					@V_B1,
    V_B2 = 					@V_B2,
    V_B6 =					@V_B6,
    V_B12 =					@V_B12,
    V_D =					@V_D,
    V_E =					@V_E,
    V_C = 					@V_C
;

-- Preferences
INSERT INTO `Preference` (category) values("Milk");
INSERT INTO `Preference` (category) values("Peanuts");
INSERT INTO `Preference` (category) values("Eggs");
INSERT INTO `Preference` (category) values("Nuts");
INSERT INTO `Preference` (category) values("Fish");
INSERT INTO `Preference` (category) values("Wheat");
INSERT INTO `Preference` (category) values("Shellfish");
INSERT INTO `Preference` (category) values("Molluscs");
INSERT INTO `Preference` (category) values("Sulfite");
INSERT INTO `Preference` (category) values("Soya");
INSERT INTO `Preference` (category) values("Sesame");
INSERT INTO `Preference` (category) values("Mustard");
INSERT INTO `Preference` (category) values("Celery");
INSERT INTO `Preference` (category) values("Lupine");
INSERT INTO `Preference` (category) values("Lactose");
INSERT INTO `Preference` (category) values("Gluten");
INSERT INTO `Preference` (category) values("Tomato");

/*Ontbijt 6:30
Ha¬ver¬mout¬pap met ba¬naan en wal¬noot*/
INSERT INTO meal (name, time, description, tip, photo_url) values(
'Havermout met banaan en walnoot','06.30','Verwarm de melk in een pan. Roer regelmatig. Voeg als de melk tegen de kook aan is al roerend de havermout en een mespunt zout toe. Draai het vuur laag en laat al roerend 5 min. indikken. Zet het vuur uit en roer de helft van de honing erdoor.
Pel ondertussen de bananen en snijd in plakjes. Breek de walnoten in stukjes. Verdeel de pap over 4 diepe borden of kommen. Verdeel de plakjes banaan en walnoten erover. Besprenkel met de rest van de honing.,Varieer ook met ander fruit, zoals (diepvries) frambozen, blauwe bessen, appel, dadels of stukjes kakifruit.', 'In plaats van walnoten kun je ook hazelnoten of amandelen gebruiken.','http://www.ah.nl.kpnis.nl/static/recepten/img_076563_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(1,255,70.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(1,189,11.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(1,127,11.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(1,182,8.0);
/*Yog¬hurt¬s¬moot¬hie met ha¬ver¬mout, ba¬naan en aard¬bei*/
INSERT INTO meal (name, time, description, tip, photo_url) values(
'Yoghurt smoothie met havermout, banaan en aardbei','06.30','Doe de halfvolle yoghurt, banaan, aardbeien en havermout in de blender en pureer 30 sec. tot een gladde smoothie. Pulseer er heel kort het geroosterd sesamzaad door.','Voeg eventueel sesamzaad toe','http://cdn.mytaste.org/i?u=https%3A%2F%2Fopdeproefbij.files.wordpress.com%2F2016%2F03%2Fyoghurtsmoothie-met-aardbei-en-havermout1.png&w=330&h=270&c=1');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(2,127,15.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(2,124,15.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(2,1005,60.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(2,189,10.0);

/*Blau¬we¬bes¬sen¬mues¬li*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Blauwebessen muesli', '06.30','Verwarm de honing 3 min. op laag vuur in een koekenpan met antiaanbaklaag en voeg de havermout toe. Roer goed, zodat alles met honing bedekt is. Voeg na 2 min. de cornflakes toe en bak 3 min. Voeg de laatste minuut de elitehaver toe. Schep op een bord en laat 10 min. afkoelen en krokant worden.
Verdeel de yoghurt over 4 kommen. Schep de bessen en de muesli over de yoghurt.','Voeg honing toe om het zoeter te maken','https://thumbs.dreamstime.com/z/muesli-met-yoghurt-en-blauwe-bessen-glaskruik-55385797.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(3,189,10.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(3,181,10.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(3,1005,65.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(3,128,15.0);

/*Spelt¬toast met hüt¬ten¬kä¬se, aard¬bei en pe¬per*/
INSERT INTO meal (name, time, description, tip, photo_url) values('toast met hüttenkäse, aardbei en peper', '06.30','Bestrijk 2 sneetjes geroosterd brood per persoon met hüttenkäse.
Snijd de aardbeien in parten en verdeel over het brood. Bestrooi met versgemalen peper.','Voeg wat peper toe','http://www.foodilove.nl/wp-content/uploads/2015/05/Spelttoast-met-huttenkase-en-aardbeien.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(4,1768,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(4,499,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(4,124,20.0);

/*Broodje kaas*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Broodje kaas', '06.30','Beleg het sneetje brood met de kaas. Verdeel de komkommer erover. Leg de tomaat op de komkommer.','Voeg ook eens ¼ eetrijpe avocado toe aan je boterham met kaas.','http://nolabelsinc.com/wp-content/uploads/2016/01/broodje-kaas.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(5,2210,60.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(5,2171,20.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(5,26,10.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(5,57,10.0);

/*Ontbijt 9:00
Ha¬ver¬mout¬pap met ba¬naan en wal¬noot*/
INSERT INTO meal (name, time, description, tip, photo_url) values(
'Havermout met banaan en walnoot','09.00','Verwarm de melk in een pan. Roer regelmatig. Voeg als de melk tegen de kook aan is al roerend de havermout en een mespunt zout toe. Draai het vuur laag en laat al roerend 5 min. indikken. Zet het vuur uit en roer de helft van de honing erdoor.
Pel ondertussen de bananen en snijd in plakjes. Breek de walnoten in stukjes. Verdeel de pap over 4 diepe borden of kommen. Verdeel de plakjes banaan en walnoten erover. Besprenkel met de rest van de honing.,Varieer ook met ander fruit, zoals (diepvries) frambozen, blauwe bessen, appel, dadels of stukjes kakifruit.', 'In plaats van walnoten kun je ook hazelnoten of amandelen gebruiken.','http://www.ah.nl.kpnis.nl/static/recepten/img_076563_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(6,255,70.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(6,189,11.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(6,127,11.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(6,182,8.0);
/*Yog¬hurt¬s¬moot¬hie met ha¬ver¬mout, ba¬naan en aard¬bei*/
INSERT INTO meal (name, time, description, tip, photo_url) values(
'Yoghurt smoothie met havermout, banaan en aardbei','09.00','Doe de halfvolle yoghurt, banaan, aardbeien en havermout in de blender en pureer 30 sec. tot een gladde smoothie. Pulseer er heel kort het geroosterd sesamzaad door.','Voeg eventueel sesamzaad toe','http://cdn.mytaste.org/i?u=https%3A%2F%2Fopdeproefbij.files.wordpress.com%2F2016%2F03%2Fyoghurtsmoothie-met-aardbei-en-havermout1.png&w=330&h=270&c=1');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(7,127,15.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(7,124,15.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(7,1005,60.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(7,189,10.0);

/*Blau¬we¬bes¬sen¬mues¬li*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Blauwebessen muesli', '09.00','Verwarm de honing 3 min. op laag vuur in een koekenpan met antiaanbaklaag en voeg de havermout toe. Roer goed, zodat alles met honing bedekt is. Voeg na 2 min. de cornflakes toe en bak 3 min. Voeg de laatste minuut de elitehaver toe. Schep op een bord en laat 10 min. afkoelen en krokant worden.
Verdeel de yoghurt over 4 kommen. Schep de bessen en de muesli over de yoghurt.','Voeg honing toe om het zoeter te maken','https://thumbs.dreamstime.com/z/muesli-met-yoghurt-en-blauwe-bessen-glaskruik-55385797.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(8,189,10.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(8,181,10.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(8,1005,65.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(8,128,15.0);

/*Spelt¬toast met hüt¬ten¬kä¬se, aard¬bei en pe¬per*/
INSERT INTO meal (name, time, description, tip, photo_url) values('toast met hüttenkäse, aardbei en peper', '09.00','Bestrijk 2 sneetjes geroosterd brood per persoon met hüttenkäse.
Snijd de aardbeien in parten en verdeel over het brood. Bestrooi met versgemalen peper.','Voeg wat peper toe','http://www.foodilove.nl/wp-content/uploads/2015/05/Spelttoast-met-huttenkase-en-aardbeien.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(9,1768,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(9,499,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(9,124,20.0);

/*Broodje kaas*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Broodje kaas', '09.00','Beleg het sneetje brood met de kaas. Verdeel de komkommer erover. Leg de tomaat op de komkommer.','Voeg ook eens ¼ eetrijpe avocado toe aan je boterham met kaas.','http://nolabelsinc.com/wp-content/uploads/2016/01/broodje-kaas.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(10,2210,60.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(10,2171,20.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(10,26,10.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(10,57,10.0);

/*Lunch 12:00
Clubsandwich*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Clubsandwich','12.00','Hak de walnoten grof en rooster ze in een droge pan 2 min. Laat ze afkoelen. Snijd de bleekselderij fijn. Meng de walnoten met de bleekselderij, de rozijnen, de mayonaise en het citroensap. Breng op smaak met peper en zout. Rooster ondertussen de sneetjes brood knapperig in de broodrooster. Laat ze een beetje afkoelen. Snijd appel in kwarten, verwijder het klokhuis en snijd de kwarten in dunne plakken.
Verdeel de waldorfsalade over 1/3 van de sneetjes brood. Besmeer de overige sneetjes met de zuivelspread. Leg de helft van de besmeerde sneetjes brood op de sneetjes met waldorfsalade, met de besmeerde kant naar boven. Snijd de ijsbergsla in dunne reepjes. Beleg de sneetjes brood met de appel en de sla. Strooi er peper naar smaak over. Bedek met de laatste sneetjes, met de besmeerde kant naar onderen. Steek door elke sandwich een tapasprikker en snijd de sandwiches doormidden.','Voeg wat rozijnen toe','http://www.simpele-recepten.nl/wp-content/uploads/2014/12/Club-sandwich.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,182,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,440,20.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,134,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,779,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,2304,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,658,15.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,1754,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(11,44,5.0);

/*Pa¬ni¬ni met moz¬za¬rel¬la, abri¬koos en pro¬s¬ci¬ut¬to*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Panini met mozzarella en abrikoos','12.00','Verwarm de paninigrill voor. Halveer ondertussen de abrikozen. Doe in een kom en giet er kokend water op tot ze onder staan. Laat 10 min. wellen. Giet af. Pluk ondertussen de blaadjes basilicum en munt van de steeltjes. Snijd de mozzarella in plakken. Snijd de panini’s in de lengte open.
Verhit de boter in een koekenpan en bak de abrikozen met de suiker 1 min. op hoog vuur. Beleg de onderste helften van de panini’s met de prosciutto, abrikozen, het basilicum, de munt en mozzarella. Breng op smaak met peper en dek af met de bovenste helft.
Leg de panini’s onder de paninigrill en gril 3 min. tot de mozzarella helemaal is gesmolten. Serveer direct.','Lekker met basilicumpesto.','http://www.ah.nl.kpnis.nl/static/recepten/img_068908_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(12,151,20.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(12,820,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(12,2388,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(12,1266,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(12,205,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(12,661,25.0);

/*Quinoa met gra¬pe¬fruit, peer en gei¬ten¬kaas*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Quinoa met gra­pe­fruit, peer en gei­ten­kaas','12.00','Kook de quinoamix volgens de aanwijzingen op de verpakking gaar in water met zout. Doe in een schaal en meng de wasabimayonaise erdoor.
Snijd de peren in kwarten en verwijder het klokhuis. Schil de parten en snijd het vruchtvlees in blokjes. Voeg toe aan de quinoa.
Snijd de rode schil en het wit van de grapefruits, zodat de partjes bloot komen te liggen. Snijd met een scherp mesje los uit de vliesjes. Voeg de helft van de partjes toe aan de quinoa.
Snijd de muntblaadjes in reepjes. Voeg met de rucola toe aan de quinoa en schep om. Breng de salade op smaak met peper en eventueel zout.
Snijd de geitenkaas in plakjes van ½ cm en leg met de rest van de grapefruit op de quinoa. Lekker met een groenteburger.','Liever iets minder pittige rucola? Kies dan voor de biologische rucola (zakje 40 g, AH Biologisch)','https://www.ah.nl.kpnis.nl/static/recepten/img_066237_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,2160,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,377,30.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,1806,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,138,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,2388,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,1794,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(13,1122,10.0);

/*mango-kip salade*/
INSERT INTO meal (name, time, description, tip, photo_url) values('mango-kip salade','12.00','Doe de bevroren mangoblokjes in een kom en meng met de kipreepjes en de sweet chilisaus. Doe in een snackdoosje.
Wanneer uw kind een paar uur later pauze heeft, is het fruit ontdooid en is de salade heerlijk koel.','De rest van de kipfiletreepjes is lekker in pasta pesto met rucola. Kijk voor het recept op ah.nl/allerhande.','http://www.ah.nl.kpnis.nl/static/recepten/img_030249_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(14,532,50.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(14,908,37.5);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(14,1244,12.5);

/*Stok­je steak*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Stok­je steak','12.00','Verwarm de oven voor op 180 °C. Verhit 2 el olie in een koekenpan en fruit de ui en paprika op laag vuur 10 min. Voeg halverwege de azijn, de tabasco en zout naar smaak toe.
Bestrijk het vlees met de rest van de olie en bestrooi met peper en zout. Verhit de grillpan en gril het vlees 4 min. Keer halverwege. Neem het vlees uit de pan en laat 5 min. rusten onder een deksel of aluminiumfolie. Snijd in dunne plakjes.
Snijd de broodjes in de lengte doormidden en rooster ze ca. 3 min. in de oven. Bestrijk de onderste helft van de broodjes met de mosterd. Verdeel het ui-paprikamengsel en vervolgens de plakjes vlees erover en bestrooi met de kaas. Lekker met kip-caesarsalade ','(recept op ah.nl/recepten)','http://www.ah.nl.kpnis.nl/static/recepten/img_007422_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,465,6.299212598425196);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,60,23.62204724409449);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,666,15.748031496062993);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,1720,2.3622047244094486);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,205,39.37007874015748);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,612,4.724409448818897);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(15,554,7.874015748031496);

/*Lunch 15:00
Clubsandwich*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Clubsandwich','15.00','Hak de walnoten grof en rooster ze in een droge pan 2 min. Laat ze afkoelen. Snijd de bleekselderij fijn. Meng de walnoten met de bleekselderij, de rozijnen, de mayonaise en het citroensap. Breng op smaak met peper en zout. Rooster ondertussen de sneetjes brood knapperig in de broodrooster. Laat ze een beetje afkoelen. Snijd appel in kwarten, verwijder het klokhuis en snijd de kwarten in dunne plakken.
Verdeel de waldorfsalade over 1/3 van de sneetjes brood. Besmeer de overige sneetjes met de zuivelspread. Leg de helft van de besmeerde sneetjes brood op de sneetjes met waldorfsalade, met de besmeerde kant naar boven. Snijd de ijsbergsla in dunne reepjes. Beleg de sneetjes brood met de appel en de sla. Strooi er peper naar smaak over. Bedek met de laatste sneetjes, met de besmeerde kant naar onderen. Steek door elke sandwich een tapasprikker en snijd de sandwiches doormidden.','Voeg wat rozijnen toe','http://www.simpele-recepten.nl/wp-content/uploads/2014/12/Club-sandwich.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,182,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,440,20.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,134,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,779,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,2304,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,658,15.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,1754,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(16,44,5.0);

/*Pa¬ni¬ni met moz¬za¬rel¬la, abri¬koos en pro¬s¬ci¬ut¬to*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Panini met mozzarella en abrikoos','15.00','Verwarm de paninigrill voor. Halveer ondertussen de abrikozen. Doe in een kom en giet er kokend water op tot ze onder staan. Laat 10 min. wellen. Giet af. Pluk ondertussen de blaadjes basilicum en munt van de steeltjes. Snijd de mozzarella in plakken. Snijd de panini’s in de lengte open.
Verhit de boter in een koekenpan en bak de abrikozen met de suiker 1 min. op hoog vuur. Beleg de onderste helften van de panini’s met de prosciutto, abrikozen, het basilicum, de munt en mozzarella. Breng op smaak met peper en dek af met de bovenste helft.
Leg de panini’s onder de paninigrill en gril 3 min. tot de mozzarella helemaal is gesmolten. Serveer direct.','Lekker met basilicumpesto.','http://www.ah.nl.kpnis.nl/static/recepten/img_068908_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(17,151,20.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(17,820,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(17,2388,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(17,1266,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(17,205,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(17,661,25.0);

/*Quinoa met gra¬pe¬fruit, peer en gei¬ten¬kaas*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Quinoa met gra­pe­fruit, peer en gei­ten­kaas','15.00','Kook de quinoamix volgens de aanwijzingen op de verpakking gaar in water met zout. Doe in een schaal en meng de wasabimayonaise erdoor.
Snijd de peren in kwarten en verwijder het klokhuis. Schil de parten en snijd het vruchtvlees in blokjes. Voeg toe aan de quinoa.
Snijd de rode schil en het wit van de grapefruits, zodat de partjes bloot komen te liggen. Snijd met een scherp mesje los uit de vliesjes. Voeg de helft van de partjes toe aan de quinoa.
Snijd de muntblaadjes in reepjes. Voeg met de rucola toe aan de quinoa en schep om. Breng de salade op smaak met peper en eventueel zout.
Snijd de geitenkaas in plakjes van ½ cm en leg met de rest van de grapefruit op de quinoa. Lekker met een groenteburger.','Liever iets minder pittige rucola? Kies dan voor de biologische rucola (zakje 40 g, AH Biologisch)','https://www.ah.nl.kpnis.nl/static/recepten/img_066237_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,2160,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,377,30.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,1806,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,138,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,2388,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,1794,5.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(18,1122,10.0);

/*mango-kip salade*/
INSERT INTO meal (name, time, description, tip, photo_url) values('mango-kip salade','15.00','Doe de bevroren mangoblokjes in een kom en meng met de kipreepjes en de sweet chilisaus. Doe in een snackdoosje.
Wanneer uw kind een paar uur later pauze heeft, is het fruit ontdooid en is de salade heerlijk koel.','De rest van de kipfiletreepjes is lekker in pasta pesto met rucola. Kijk voor het recept op ah.nl/allerhande.','http://www.ah.nl.kpnis.nl/static/recepten/img_030249_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(19,532,50.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(19,908,37.5);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(19,1244,12.5);

/*Stok­je steak*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Stok­je steak','15.00','Verwarm de oven voor op 180 °C. Verhit 2 el olie in een koekenpan en fruit de ui en paprika op laag vuur 10 min. Voeg halverwege de azijn, de tabasco en zout naar smaak toe.
Bestrijk het vlees met de rest van de olie en bestrooi met peper en zout. Verhit de grillpan en gril het vlees 4 min. Keer halverwege. Neem het vlees uit de pan en laat 5 min. rusten onder een deksel of aluminiumfolie. Snijd in dunne plakjes.
Snijd de broodjes in de lengte doormidden en rooster ze ca. 3 min. in de oven. Bestrijk de onderste helft van de broodjes met de mosterd. Verdeel het ui-paprikamengsel en vervolgens de plakjes vlees erover en bestrooi met de kaas. Lekker met kip-caesarsalade ','(recept op ah.nl/recepten)','http://www.ah.nl.kpnis.nl/static/recepten/img_007422_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,465,6.299212598425196);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,60,23.62204724409449);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,666,15.748031496062993);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,1720,2.3622047244094486);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,205,39.37007874015748);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,612,4.724409448818897);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(20,554,7.874015748031496);

/* Diner 19.00
Healthy hamburger*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Healthy hamburger','19.00','Snijd de courgettes en tomaten in plakjes van ½ cm en snijd de knoflook fijn. Doe 6 el olie met de knoflook en de helft van de rucola in een hoge beker en pureer met de staafmixer. Breng op smaak met (versgemalen) peper.
Verhit de grillpan. Bestrijk de plakjes courgette en tomaat met de rucolaolie en gril de courgette in 4 min. goudbruin en keer halverwege.
Gril de tomatenplakjes 1 min. en keer halverwege. Verdeel de rest van de rucolaolie over de groenten en schep om zodat ze rondom bedekt zijn. Laat de grillpan op het vuur staan.
Bestrijk de tartaartjes met de rest van de olijfolie en bestrooi met peper en eventueel zout. Gril in 10 min. rosé. Keer halverwege.
Halveer ondertussen de broodjes en bestrijk de onderste helften met de hummus. Verdeel de rest van de rucola over de broodjes en beleg met een paar plakjes courgette en tomaat. Leg op elk een tartaartje en schep er 1 el salsa op.
Beleg met nog wat courgette en tomaat. Dek af met de bovenkant van het broodje. Serveer met de rest van de courgette en tomaat.','Voeg voor extra pit 1 el jalapeños (pepers) toe aan de rucolaolie voordat je deze pureert.','http://www.ah.nl.kpnis.nl/static/recepten/img_057254_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,681,7.6923076923076925);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,57,15.384615384615385);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,618,1.5384615384615385);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,465,5.384615384615385);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,1794,3.076923076923077);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,1041,30.76923076923077);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,1841,23.076923076923077);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,2209,6.153846153846154);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(21,1719,6.923076923076923);

/*Rijstsalade met tonijn*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Rijstsalade met tonijn','19.00','Kook de rijst volgens de aanwijzingen op de verpakking.Kook ondertussen de bimi 3 min. in kokend water beetgaar.
Boen ondertussen de limoen schoon, rasp de groene schil van de limoen en pers de vrucht uit. Snijd het steeltje van de rode peper. Halveer de peper in de lengte en verwijder met een scherp mesje de zaadlijsten. Snijd het vruchtvlees fijn.
Meng het limoenrasp en -sap met de rode peper, sojasaus en sesamolie tot een dressing. Snijd de koriander fijn. Snijd de avocado overlangs doormidden. Verwijder de pit, schep het vruchtvlees met een lepel uit de schil en snijd het in blokjes. Meng de dressing door de rijst. Schep de bimi, koriander, avocado en tonijn erdoor. Bestrooi met het sesamzaad.','Sojasaus toevoegen','http://www.ah.nl.kpnis.nl/static/recepten/img_060017_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,543,31.07344632768362);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,531,5.649717514124294);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,824,2.2598870056497176);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,2387,2.2598870056497176);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,529,11.299435028248588);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,1075,41.80790960451977);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(22,625,5.649717514124294);

/*Kiphaasjes met notenkorst*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Kiphaasjes met notenkorst, sper­zie­bo­nen en aard­ap­pel','19.00','Doe de noten in de keukenmachine en maal bijna fijn. Doe 2/3 in een diep bord. Bewaar de rest van het notenkruim. Doe de bloem en het ei elk apart in een diep bord. Haal de kip achtereenvolgens door de bloem, het ei en de notenkruim. Verhit 2 el olie in een koekenpan en bak de kip rondom in 8 min. op middelhoog vuur goudbruin en gaar.
Kook ondertussen de sperziebonen 5 min. Giet af en laat goed uitlekken. Bereid de stoomaardappelen volgens de aanwijzingen op de verpakking in de magnetron.
Verhit ondertussen de rest van de olie in een hapjespan en bak de ui met wat peper 5 min. op middelhoog vuur.
Voeg de sperziebonen en de rest van de notenkruim toe aan de ui en bak 2 min. mee. Serveer de aardappelen met de groente en de kip.','Je kunt de noten ook met een mes fijnhakken. Je bent dan wel iets langer bezig dan 17 minuten.','http://www.ah.nl.kpnis.nl/static/recepten/img_076566_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,183,6.666666666666667);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,76,2.666666666666667);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,1567,1.3333333333333335);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,1736,16.88888888888889);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,276,3.5555555555555554);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,48,22.22222222222222);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,1,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(23,60,6.666666666666667);

/*Zalmpakketje uit de oven*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Zalmpakketje uit de oven','19.00','Verwarm de oven voor op 200 °C. Halveer de preien in de lengte en snijd in 4 gelijke stukken.
Verwijder de steelaanzet van de haricorts verts en blancheer de bonen 3 min. in kokend water. Spoel koud en verdeel over de stukken bakpapier. Verdeel de prei en aardappelpartjes erover en bestrooi met peper en eventueel zout.
Leg hierop de zalmfilets en bestrooi met de picadillo. Voeg eventueel zout toe. Vouw de pakketjes losjes maar goed dicht, zodat er geen stoom kan ontsnappen. Leg de pakketjes op een bakplaat en bak ca. 20 min. in de oven. Snijd de limoen in parten en serveer bij de pakketjes.','bakpapier gebruiken','http://www.ah.nl.kpnis.nl/static/recepten/img_057286_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(24,445,7.894736842105263);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(24,35,6.578947368421052);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(24,697,23.026315789473685);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(24,1,26.31578947368421);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(24,1072,32.89473684210527);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(24,531,3.289473684210526);

/*Bieflapjes met spruiten en zoeteaardappelpuree*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Bieflapjes met spruiten en zoeteaardappelpuree','19.00','Schil de aardappelen, snijd in gelijke stukken en kook in een pan water met zout in 10 min. gaar.
Kook ondertussen de spruiten in een pan water in 6 min. beetgaar.
Dep ondertussen het vlees droog met keukenpapier en bestrooi met peper en zout. Verhit de olie in een koekenpan en bak het vlees in 5 min. goudbruin. Neem uit de pan en dek af met aluminiumfolie. Hak de walnoten grof.
Giet de spruiten af en schep om met de mosterd en walnoten. Giet de aardappelen af en stamp met de pureestamper tot een grove puree. Breng op smaak met peper en eventueel zout. Serveer de bieflapjes met de zoete-aardappelpuree en spruiten.','Tutorial kijken voor de biefstuk','http://www.ah.nl.kpnis.nl/static/recepten/img_076546_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(25,2,53.475935828877006);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(25,810,26.737967914438503);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(25,915,16.0427807486631);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(25,465,1.6042780748663104);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(25,182,2.13903743315508);

/* Diner 17.30
Healthy hamburger*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Healthy hamburger','17.30','Snijd de courgettes en tomaten in plakjes van ½ cm en snijd de knoflook fijn. Doe 6 el olie met de knoflook en de helft van de rucola in een hoge beker en pureer met de staafmixer. Breng op smaak met (versgemalen) peper.
Verhit de grillpan. Bestrijk de plakjes courgette en tomaat met de rucolaolie en gril de courgette in 4 min. goudbruin en keer halverwege.
Gril de tomatenplakjes 1 min. en keer halverwege. Verdeel de rest van de rucolaolie over de groenten en schep om zodat ze rondom bedekt zijn. Laat de grillpan op het vuur staan.
Bestrijk de tartaartjes met de rest van de olijfolie en bestrooi met peper en eventueel zout. Gril in 10 min. rosé. Keer halverwege.
Halveer ondertussen de broodjes en bestrijk de onderste helften met de hummus. Verdeel de rest van de rucola over de broodjes en beleg met een paar plakjes courgette en tomaat. Leg op elk een tartaartje en schep er 1 el salsa op.
Beleg met nog wat courgette en tomaat. Dek af met de bovenkant van het broodje. Serveer met de rest van de courgette en tomaat.','Voeg voor extra pit 1 el jalapeños (pepers) toe aan de rucolaolie voordat je deze pureert.','http://www.ah.nl.kpnis.nl/static/recepten/img_057254_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,681,7.6923076923076925);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,57,15.384615384615385);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,618,1.5384615384615385);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,465,5.384615384615385);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,1794,3.076923076923077);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,1041,30.76923076923077);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,1841,23.076923076923077);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,2209,6.153846153846154);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(26,1719,6.923076923076923);

/*Rijstsalade met tonijn*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Rijstsalade met tonijn','17.30','Kook de rijst volgens de aanwijzingen op de verpakking.Kook ondertussen de bimi 3 min. in kokend water beetgaar.
Boen ondertussen de limoen schoon, rasp de groene schil van de limoen en pers de vrucht uit. Snijd het steeltje van de rode peper. Halveer de peper in de lengte en verwijder met een scherp mesje de zaadlijsten. Snijd het vruchtvlees fijn.
Meng het limoenrasp en -sap met de rode peper, sojasaus en sesamolie tot een dressing. Snijd de koriander fijn. Snijd de avocado overlangs doormidden. Verwijder de pit, schep het vruchtvlees met een lepel uit de schil en snijd het in blokjes. Meng de dressing door de rijst. Schep de bimi, koriander, avocado en tonijn erdoor. Bestrooi met het sesamzaad.','Sojasaus toevoegen','http://www.ah.nl.kpnis.nl/static/recepten/img_060017_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,543,31.07344632768362);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,531,5.649717514124294);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,824,2.2598870056497176);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,2387,2.2598870056497176);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,529,11.299435028248588);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,1075,41.80790960451977);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(27,625,5.649717514124294);

/*Kiphaasjes met notenkorst*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Kiphaasjes met notenkorst, sper­zie­bo­nen en aard­ap­pel','17.30','Doe de noten in de keukenmachine en maal bijna fijn. Doe 2/3 in een diep bord. Bewaar de rest van het notenkruim. Doe de bloem en het ei elk apart in een diep bord. Haal de kip achtereenvolgens door de bloem, het ei en de notenkruim. Verhit 2 el olie in een koekenpan en bak de kip rondom in 8 min. op middelhoog vuur goudbruin en gaar.
Kook ondertussen de sperziebonen 5 min. Giet af en laat goed uitlekken. Bereid de stoomaardappelen volgens de aanwijzingen op de verpakking in de magnetron.
Verhit ondertussen de rest van de olie in een hapjespan en bak de ui met wat peper 5 min. op middelhoog vuur.
Voeg de sperziebonen en de rest van de notenkruim toe aan de ui en bak 2 min. mee. Serveer de aardappelen met de groente en de kip.','Je kunt de noten ook met een mes fijnhakken. Je bent dan wel iets langer bezig dan 17 minuten.','http://www.ah.nl.kpnis.nl/static/recepten/img_076566_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,183,6.666666666666667);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,76,2.666666666666667);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,1567,1.3333333333333335);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,1736,16.88888888888889);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,276,3.5555555555555554);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,48,22.22222222222222);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,1,40.0);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(28,60,6.666666666666667);

/*Zalmpakketje uit de oven*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Zalmpakketje uit de oven','17.30','Verwarm de oven voor op 200 °C. Halveer de preien in de lengte en snijd in 4 gelijke stukken.
Verwijder de steelaanzet van de haricorts verts en blancheer de bonen 3 min. in kokend water. Spoel koud en verdeel over de stukken bakpapier. Verdeel de prei en aardappelpartjes erover en bestrooi met peper en eventueel zout.
Leg hierop de zalmfilets en bestrooi met de picadillo. Voeg eventueel zout toe. Vouw de pakketjes losjes maar goed dicht, zodat er geen stoom kan ontsnappen. Leg de pakketjes op een bakplaat en bak ca. 20 min. in de oven. Snijd de limoen in parten en serveer bij de pakketjes.','bakpapier gebruiken','http://www.ah.nl.kpnis.nl/static/recepten/img_057286_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(29,445,7.894736842105263);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(29,35,6.578947368421052);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(29,697,23.026315789473685);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(29,1,26.31578947368421);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(29,1072,32.89473684210527);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(29,531,3.289473684210526);

/*Bieflapjes met spruiten en zoeteaardappelpuree*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Bieflapjes met spruiten en zoeteaardappelpuree','17.30','Schil de aardappelen, snijd in gelijke stukken en kook in een pan water met zout in 10 min. gaar.
Kook ondertussen de spruiten in een pan water in 6 min. beetgaar.
Dep ondertussen het vlees droog met keukenpapier en bestrooi met peper en zout. Verhit de olie in een koekenpan en bak het vlees in 5 min. goudbruin. Neem uit de pan en dek af met aluminiumfolie. Hak de walnoten grof.
Giet de spruiten af en schep om met de mosterd en walnoten. Giet de aardappelen af en stamp met de pureestamper tot een grove puree. Breng op smaak met peper en eventueel zout. Serveer de bieflapjes met de zoete-aardappelpuree en spruiten.','Tutorial kijken voor de biefstuk','http://www.ah.nl.kpnis.nl/static/recepten/img_076546_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(30,2,53.475935828877006);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(30,810,26.737967914438503);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(30,915,16.0427807486631);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(30,465,1.6042780748663104);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(30,182,2.13903743315508);

/*Snack 19.30
tropisch sapje*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Tropisch sapje','19.30','Schenk het ananassap in een blender of keukenmachine. Voeg de banaan en het vruchtvlees van de mango toe aan het sap. Mix 1 min. en schenk het sap in 2 grote glazen.','Lekker','http://cdn.mytaste.org/i?u=http%3A%2F%2Fcmgtcontent.ahold.com.kpnis.nl%2Fcmgtcontent%2Fmedia%2F%2F000614900%2F000%2F000614940_001_FRAL10021453_300.jpg&w=330&h=270&c=1');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(31,970,45.45454545454545);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(31,127,27.27272727272727);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(31,532,27.27272727272727);

/*appel*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Appel','19.30','Was de appel in koud kraanwater.','Lekker fris','https://www.voedingswaardetabel.nl/_lib/img/prod/big/appels.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(32,658,100.0);

/*banaan*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Banaan','19.30','Open de banaan vanaf de onderkant','Als hij een beetje bruin is is hij zelfs nog gezonder!','https://www.voedingswaardetabel.nl/_lib/img/prod/big/banaan.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(33,127,100.0);

/*tomaatjes met yoghurt saus*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Snacktomaatjes met frisse yoghurtdip','19.30','Meng de yoghurt, mayonaise en de mosterd in een kommetje. Meng de bieslook door de saus. Prik de tomaten aan een tapasprikker en serveer met de yoghurtdip.','Gebruik prikkers :)','http://www.ah.nl.kpnis.nl/static/recepten/img_007266_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(34,1005,7.936507936507936);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(34,377,7.936507936507936);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(34,612,1.5873015873015872);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(34,617,3.1746031746031744);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(34,57,79.36507936507937);

/*wortel*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Wortel','19.30','Was ze onder koud kraanwater.','gezonder zonder dip!','http://www.uzuma.nl/img/cms/Uzuma/Ingredients/Carrot/NL/Wortel1.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(35,67,100.0);

/*Snack 22.00 */

/*appel*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Appel','22.00','Was de appel in koud kraanwater.','Lekker fris','https://www.voedingswaardetabel.nl/_lib/img/prod/big/appels.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(36,658,100.0);

/*banaan*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Banaan','22.00','Open de banaan vanaf de onderkant','Als hij een beetje bruin is is hij zelfs nog gezonder!','https://www.voedingswaardetabel.nl/_lib/img/prod/big/banaan.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(37,127,100.0);

/*tomaatjes met yoghurt saus*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Snacktomaatjes met frisse yoghurtdip','22.00','Meng de yoghurt, mayonaise en de mosterd in een kommetje. Meng de bieslook door de saus. Prik de tomaten aan een tapasprikker en serveer met de yoghurtdip.','Gebruik prikkers :)','http://www.ah.nl.kpnis.nl/static/recepten/img_007266_445x297_JPG.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(38,1005,7.936507936507936);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(38,377,7.936507936507936);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(38,612,1.5873015873015872);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(38,617,3.1746031746031744);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(38,57,79.36507936507937);

/*wortel*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Wortel','22.00','Was ze onder koud kraanwater.','gezonder zonder dip!','http://www.uzuma.nl/img/cms/Uzuma/Ingredients/Carrot/NL/Wortel1.jpg');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(39,67,100.0);

/* tropisch sapje*/
INSERT INTO meal (name, time, description, tip, photo_url) values('Tropisch sapje','22.00','Schenk het ananassap in een blender of keukenmachine. Voeg de banaan en het vruchtvlees van de mango toe aan het sap. Mix 1 min. en schenk het sap in 2 grote glazen.','Lekker','http://cdn.mytaste.org/i?u=http%3A%2F%2Fcmgtcontent.ahold.com.kpnis.nl%2Fcmgtcontent%2Fmedia%2F%2F000614900%2F000%2F000614940_001_FRAL10021453_300.jpg&w=330&h=270&c=1');
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(40,970,45.45454545454545);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(40,127,27.27272727272727);
INSERT INTO meal_food (meal_id, food_id, percentage_of_food_in_meal) values(40,532,27.27272727272727);
