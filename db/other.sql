call getMeals('06.30', '09.00', '12.00', '15.00', '17.30', '19.00', '19.30', '22.00');

call getNextMeals('06.30', 1, 100);

call getMealIngredients(1);

-- 
-- SELECT 
-- 	m.*, 
-- 	ROUND(SUM(f.kj * mf.percentage_of_food_in_meal), 2) AS kj,  
-- 	ROUND(SUM(f.kcal * mf.percentage_of_food_in_meal), 2) AS kcal,
-- 	ROUND(SUM(f.eiwit * mf.percentage_of_food_in_meal), 2) AS protein,
-- 	ROUND(SUM(f.koolhydraten * mf.percentage_of_food_in_meal), 2) AS carbohydrates,
-- 	ROUND(SUM(f.vet * mf.percentage_of_food_in_meal), 2) AS fat,
-- 	@count:=IF(@time = time, @count + 1, 1) AS count,
-- 	@time:= time AS time
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- AND m.time IN ('06.30', '09.00', '12.00', '15.00', '17.30', '19.00', '19.30', '22.00')
-- GROUP BY m.id
-- HAVING count <= 3;

-- call getMealIngredients(1);
-- 
-- call getMeals();
-- 
-- call getNextMeals('06.30', 3, 5);


-- SELECT f.*
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- AND m.id = 1;

-- SELECT 
-- 	m.*, 
-- 	SUM(f.kj * mf.percentage_of_food_in_meal) AS kj,  
-- 	SUM(f.kcal * mf.percentage_of_food_in_meal) AS kcal,
-- 	SUM(f.eiwit * mf.percentage_of_food_in_meal) AS protein,
-- 	SUM(f.koolhydraten * mf.percentage_of_food_in_meal) AS carbohydrates,
-- 	SUM(f.vet * mf.percentage_of_food_in_meal) AS fat
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- AND sort = 'lunch'
-- AND m.id > 11
-- GROUP BY m.id
-- LIMIT 5;

-- SELECT 
-- 	m.*, 
-- 	SUM(f.kj * mf.percentage_of_food_in_meal) AS kj,  
-- 	SUM(f.kcal * mf.percentage_of_food_in_meal) AS kcal,
-- 	SUM(f.eiwit * mf.percentage_of_food_in_meal) AS protein,
-- 	SUM(f.koolhydraten * mf.percentage_of_food_in_meal) AS carbohydrates,
-- 	SUM(f.vet * mf.percentage_of_food_in_meal) AS fat,
--     @count:=IF(@sort = sort, @count + 1, 1) AS count,
-- 	@sort:=sort AS sort
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- GROUP BY m.id
-- HAVING count <= 5;

-- SELECT 
-- 	m.*,
--     mf.*,
--     f.*
-- FROM (
-- 	SELECT 
-- 		id,
--         name,
--         description,
--         tip,
--         photo_url,
-- 		@count:=IF(@sort = sort, @count + 1, 1) AS count,
-- 		@sort:=sort AS sort
-- 	FROM meal) AS 
--     m,
-- 	meal_food mf, 
-- 	food_nl f
-- WHERE count <= 5;
-- 
--
-- SELECT 
-- 	m.*, 
-- 	SUM(f.kj * mf.percentage_of_food_in_meal) AS kj,  
-- 	SUM(f.kcal * mf.percentage_of_food_in_meal) AS kcal,
-- 	SUM(f.eiwit * mf.percentage_of_food_in_meal) AS protein,
-- 	SUM(f.koolhydraten * mf.percentage_of_food_in_meal) AS carbohydrates,
-- 	SUM(f.vet * mf.percentage_of_food_in_meal) AS fat,
--     @count:=IF(@sort = sort, @count + 1, 1) AS count,
-- 	@sort:=sort AS sort
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- GROUP BY m.id
-- HAVING count <= 5;


-- SELECT 
--     id, name
-- FROM
--     (SELECT 
--         id,
--             name,
--             @count:=IF(@sort = sort, @count + 1, 1) AS count,
--             @sort:=sort AS sort
--     FROM
--         meal) AS count WHERE count <= 5;
--
-- SELECT 
-- 	m.*, 
-- 	SUM(f.kj * mf.percentage_of_food_in_meal) AS kj,  
-- 	SUM(f.kcal * mf.percentage_of_food_in_meal) AS kcal,
-- 	SUM(f.eiwit * mf.percentage_of_food_in_meal) AS protein,
-- 	SUM(f.koolhydraten * mf.percentage_of_food_in_meal) AS carbohydrates,
-- 	SUM(f.vet * mf.percentage_of_food_in_meal) AS fat
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- GROUP BY m.id;


-- SET 
-- 	@currcount = 1, 
--     @currvalue = 1;
-- SELECT 
-- 	m.*, 
-- 	SUM(f.kj * mf.percentage_of_food_in_meal) AS kj,  
-- 	SUM(f.kcal * mf.percentage_of_food_in_meal) AS kcal,
-- 	SUM(f.eiwit * mf.percentage_of_food_in_meal) AS protein,
-- 	SUM(f.koolhydraten * mf.percentage_of_food_in_meal) AS carbohydrates,
-- 	SUM(f.vet * mf.percentage_of_food_in_meal) AS fat,
--     @currcount := IF(@currvalue = m.id, @currcount + 1, 1) AS rank,
--     @currvalue := m.id
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- GROUP BY m.id;
-- 

-- SET 
-- 	@cursort = NULL, 
--     @sortcount = 1;
-- SELECT
-- 	id, name, sort,
-- 	@cursort := sort AS cursort,
--     IF(@cursort = sort, @sortcount := @sortcount + 1, 1) AS sortcount
-- FROM meal;


-- SET 
-- 	@cursort = NULL, 
--     @sortcount = 1;
-- SELECT
-- 	id, name, sort,
-- 	@cursort := sort AS cursort,
--     IF(@cursort = sort, @sortcount := @sortcount + 1, 1) AS sortcount
-- FROM meal;
-- 

-- SELECT
-- 	id,
-- 	name,
-- 	@count := IF (@sort = sort, @count + 1, 1) AS count,
-- 	@sort := sort AS sort
-- FROM 
-- 	meal;
--
-- SET 
-- 	@currcount = NULL, 
--     @currvalue = NULL;
-- SELECT
-- 	id, name, sort, rank 
-- FROM (
-- 	SELECT id, name, sort,
-- 		@currcount := IF(@currvalue = id, @currcount + 1, 1) AS rank,
-- 		@currvalue := id AS whatever
-- 	FROM meal) AS whatever;

-- SET @currcount = NULL, @currvalue = NULL;
-- SELECT id, year, rate FROM (
--     SELECT
--         id, year, rate, 
--         @currcount := IF(@currvalue = id, @currcount + 1, 1) AS rank,
--         @currvalue := id AS whatever
--     FROM test
--     ORDER BY id, rate DESC
-- ) AS whatever WHERE rank <= 5


-- 
-- SET @currcount = NULL, @currvalue = NULL;
-- SELECT
--     id, year, rate, 
--     @currcount := IF(@currvalue = id, @currcount + 1, 1) AS rank,
--     @currvalue := id AS whatever
-- FROM test
-- ORDER BY id, rate DESC;


-- SELECT Meal.NameMeal, Meal.SortMeal, Meal.id, SUM(Food_NL.kcal) as kcal
-- FROM Meal, Meal_Food, Food_NL
-- WHERE Meal.id = Meal_Food.Meal_id
-- AND Food_NL.id = Meal_Food.Food_id
-- AND Meal.SortMeal = 'dinner'
-- GROUP BY Meal.id
-- HAVING SUM(Food_NL.kcal) BETWEEN 300 AND 1200;
-- 
-- SELECT 
-- 	m.*, 
--     SUM(f.kj ) as energy,
--     SUM(f.kcal) as kcal,
--     SUM(f.eiwit) as protein,
--     SUM(f.Koolhydraten) as carbohydrates,
--     SUM(f.vet ) as fat
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- GROUP BY m.id;

-- SELECT 
-- 	m.*, 
--     SUM(f.kj ) as energy,                 -- <- should be based on the percentage
--     SUM(f.kcal) as kcal,				  -- <- should be based on the percentage
--     SUM(f.eiwit) as protein,
--     SUM(f.Koolhydraten) as carbohydrates,
--     SUM(f.vet ) as fat
-- FROM meal m, meal_food mf, food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- GROUP BY m.id;

-- SELECT m.namemeal, mf.percentageoffoodinmeal
-- FROM meal m, meal_food mf
-- WHERE m.id = mf.meal_id
-- AND m.id = 1;

-- SELECT 
-- 	m.namemeal, 
--     mf.percentageoffoodinmeal,
--     f.kcal,
--     f.kj
-- FROM 
-- 	meal m, 
--     meal_food mf,
--     food_nl f
-- WHERE m.id = mf.meal_id
-- AND f.id = mf.food_id
-- AND m.id = 1;

--  SELECT * 
--  FROM Food_NL
--  WHERE Food_NL.id IN (
-- 	SELECT Food_id 
--     FROM Meal_Food
--     WHERE Meal_id = 4);

-- DELIMITER $$
-- DROP PROCEDURE IF EXISTS `debug_msg`$$
-- DROP FUNCTION IF EXISTS `get_meals`$$
-- 
-- CREATE PROCEDURE debug_msg(enabled INTEGER, msg VARCHAR(255))
-- BEGIN
--   IF enabled THEN BEGIN
--     select concat("** ", msg) AS '** DEBUG:';
--   END; END IF;
-- END $$
-- 
-- CREATE PROCEDURE get_meals(
-- 	IN need INTEGER,
--     IN sortMeal VARCHAR(255),
--     OUT amount INT)
-- BEGIN
-- 	SELECT count(*) INTO amount
--     FROM Meal
--     WHERE SortMeal = sortMeal;
-- END $$
-- DELIMITER ;
-- 
-- 
-- CALL get_meals(200, 'Ontbijt', @amount);
-- SELECT @amount;

-- SELECT SUM(Eiwit * meal_food.PercentageOfFoodInMeal) AS Eiwit FROM FOOD_NL, meal_food WHERE food_nl.ID = meal_food.Food_ID AND ID IN (SELECT food_id FROM Meal_Food WHERE Meal_ID IN (SELECT ID FROM Meal WHERE ID = 1));
-- 
-- SELECT COUNT(*) FROM FOOD_NL;
-- 
/*SELECT *FROM meal AS m1 JOIN(SELECT CEIL(RAND() * (SELECT MAX(id) - 5 FROM meal WHERE SortMeal = 'Ontbijt')) AS id) AS m2 WHERE m1.id >= m2.id AND m1.SortMeal = 'Ontbijt' ORDER BY m1.id ASC LIMIT 5;*/
/*SELECT * FROM FOOD_NL*/

/*SELECT *
FROM meal
WHERE SortMeal = 'Ontbijt' 
AND 


SELECT   * 
FROM     meal AS m1 
JOIN 
         ( 
                SELECT Ceil(Rand() * 
                       ( 
                              SELECT Max(id) - 5 
                              FROM   meal 
                              WHERE  id IN 
                                     ( 
                                            SELECT user_id 
                                            FROM   user_preference 
                                            WHERE  user_id = 1 
                                            AND    min_count <= plus_count))) AS id) AS m2 

WHERE m1.sortmeal = 'Ontbijt' 
limit 5;

UPDATE USER_PREFERENCE set MIN_COUNT = 10 WHERE USER_ID = 1 AND Preference_ID = 1;

SELECT * FROM user_preference;

SELECT * FROM USER;*/

/* van een maaltijd de voedingswaarde ophalen*/

-- SELECT * FROM MEAL;
-- 
-- SELECT * FROM FOOD_NL;