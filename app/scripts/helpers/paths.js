import path from 'path';

export const srcDir = 'src';
export const buildDir = 'build';
export const stylesDir = 'styles';

export const basePath = path.resolve(__dirname, '../../');
export const srcPath = path.join(basePath, srcDir);
export const buildPath = path.join(basePath, buildDir);

export const stylesPath = path.resolve(srcPath, stylesDir);
