import browserSync from 'browser-sync';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import history from 'connect-history-api-fallback';

import { srcDir } from './helpers/paths';
import webpackConfig from './webpackConfig';

const bundler = webpack(webpackConfig);

browserSync({
  logFileChanges: false,
  notify: false,
  server: {
    baseDir: srcDir,
    middleware: [
      history(),
      webpackDevMiddleware(bundler, {
        publicPath: webpackConfig.output.publicPath,
        quiet: false,
        stats: {
          chunks: false,
          chunkModules: false,
          colors: true
        }
      }),
      webpackHotMiddleware(bundler)
    ]
  },
  files: [
    srcDir + '/*.html',
    srcDir + '/*.css'
  ]
});
