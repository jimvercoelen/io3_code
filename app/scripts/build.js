import path from 'path';
import webpack from 'webpack';
import webpackConfig from './webpackConfig';
import { srcPath, buildPath } from './helpers/paths';
import fs from 'fs-extra';

const compiler = webpack(webpackConfig);

compiler.run((error) => {
  if (error) {
    return console.log(error);
  }

  console.log('Compiled. Now copying static assets to the build folder.');
  fs.copySync(path.join(srcPath, 'static'), path.join(buildPath, 'static'));
  console.log('Finished copying static assets.');
});
