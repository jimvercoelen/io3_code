import { srcPath } from '../helpers/paths';

export default function (webpackConfig) {
  webpackConfig.module.loaders.push({
    test: /\.js$/,
    loader: 'babel',
    include: [srcPath],
    query: {
      cacheDirectory: true
    }
  });

  webpackConfig.module.loaders.push({
    test: /\.json$/,
    loader: 'json'
  });
}
