import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { env, isProduction, isDevelopment, isTest } from '../helpers/env';
import { srcPath } from '../helpers/paths';

const html = {
  template: path.resolve(srcPath, 'index.html'),
  hash: false,
  filename: 'index.html',
  inject: 'body',
  // favicon: path.resolve(srcPath, 'static/favicon.ico'),
  minify: {
    collapseWhitespace: true
  }
};

const globals = {
  'process.env': {
    'NODE_ENV': JSON.stringify(env)
  },
  'NODE_ENV': env,
  '__DEV__': isDevelopment,
  '__PROD__': isProduction,
  '__TEST__': isTest
};

export default function (webpackConfig) {
  webpackConfig.plugins.push(
    new webpack.DefinePlugin(globals),
    new HtmlWebpackPlugin(html)
  );

  if (isDevelopment) {
    webpackConfig.plugins.push(
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin()
    );
  } else if (isProduction) {
    webpackConfig.plugins.push(
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          unused: true,
          warnings: false,
          screw_ie8: true,
          drop_console: true,
          drop_debugger: true,
          dead_code: true
        }
      })
    );
  }

  if (!isTest) {
    webpackConfig.plugins.push(new webpack.optimize.CommonsChunkPlugin({
      names: ['vendors']
    }));
  }

  return webpackConfig;
}
