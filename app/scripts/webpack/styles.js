import cssnano from 'cssnano';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import { stylesPath } from '../helpers/paths';
import { isDevelopment } from '../helpers/env';

export default function (webpackConfig) {
  const cssLoader = 'css?sourceMap&-minimize&importLoaders=1';

  webpackConfig.module.loaders.push({
    test: /\.scss$/,
    loaders: [
      'style',
      cssLoader,
      'postcss',
      'sass?sourceMap'
    ]
  });

  webpackConfig.module.loaders.push({
    test: /\.css$/,
    loaders: [
      'style',
      cssLoader,
      'postcss'
    ]
  });

  webpackConfig.sassLoader = {
    includePaths: stylesPath
  };

  webpackConfig.postcss = [
    cssnano({
      autoprefixer: {
        add: true,
        remove: true,
        browsers: ['last 2 versions']
      },
      discardComments: {
        removeAll: true
      },
      safe: true,
      sourcemap: true
    })
  ];

  if (!isDevelopment) {
    // when we don't know the public path (we know it only when HMR is enabled [in development]) we
    // need to use the extractTextPlugin to fix this issue:
    // http://stackoverflow.com/questions/34133808/webpack-ots-parsing-error-loading-fonts/34133809#34133809
    webpackConfig.module.loaders.filter((loader) =>
      loader.loaders && loader.loaders.find((name) => /css/.test(name.split('?')[0]))
    ).forEach((loader) => {
      const [first, ...rest] = loader.loaders;
      loader.loader = ExtractTextPlugin.extract(first, rest.join('!'));
      delete loader.loaders;
    });

    webpackConfig.plugins.push(
      new ExtractTextPlugin('[name].[contenthash].css', {
        allChunks: true
      })
    );
  }
}
