import browserSync from 'browser-sync';
import history from 'connect-history-api-fallback';
import compression from 'compression';

import { buildDir } from './helpers/paths';

browserSync({
  minify: false,
  server: {
    baseDir: buildDir,
    middleware: [
      history(),
      compression()
    ]
  }
});
