import path from 'path';

import { isDevelopment, isProduction } from './helpers/env';
import { srcPath, buildPath } from './helpers/paths';
import vendors from '../vendors.json';

import plugins from './webpack/plugins';
import images from './webpack/images';
import scripts from './webpack/scripts';
import styles from './webpack/styles';

const publicPath = '/';
const debug = false;
const devtool = (isProduction) ? null : 'source-map';
const hashType = (isProduction) ? 'chunkhash' : 'hash';

const mainModulePath = path.resolve(srcPath, 'index.js');

const app = isDevelopment
  ? [
    'webpack/hot/dev-server',
    'webpack-hot-middleware/client',
    'react-hot-loader/patch',
    mainModulePath
  ] : [mainModulePath];

let webpackConfig = {
  debug,
  name: 'client',
  target: 'web',
  devtool,
  entry: {
    app,
    vendors
  },
  output: {
    filename: `[name]-[${hashType}].js`,
    path: buildPath,
    publicPath: publicPath
  },
  resolve: {
    root: srcPath,
    extension: ['', '.js']
  },
  plugins: [],
  module: {
    loaders: [],
    preLoaders: []
  }
};

plugins(webpackConfig);
scripts(webpackConfig);
images(webpackConfig);
styles(webpackConfig);

export default webpackConfig;
