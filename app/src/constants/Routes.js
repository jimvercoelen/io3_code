export const ROUTE_HOME = '/';
export const ROUTE_LOGIN = '/login';
export const ROUTE_LOGOUT = '/logout';
export const ROUTE_CREATE_ACCOUNT = '/create-account';
export const ROUTE_PROFILE = '/profile';
export const ROUTE_FOOD = '/food';
