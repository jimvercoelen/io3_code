export const isValidEmail = email => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);

export const isValidPassword = password => password && password.length > 5;

export const isEmpty = input => input && input !== '';

export const isFieldEmpty = field => !field.value || !isEmpty(field.value);
