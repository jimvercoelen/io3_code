export const setAuth = (id) => {
  localStorage.setItem('auth', id);
};

export const getAuth = () => {
  return localStorage.getItem('auth');
};

export const removeAuth = () => {
  localStorage.removeItem('auth');
};
