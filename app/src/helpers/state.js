export const isLoggedIn = state => {
  if (!state.user.auth) return false;
  if (!state.user.auth.email) return false;
  if (state.user.auth.email === 'undefined') return false;
  return true;
};

export const isProfileSet = state => {
  if (!state.user.auth) return false;
  if (!state.user.auth.need) return false;
  if (state.user.auth.need === 'undefined') return false;
  return true;
};


