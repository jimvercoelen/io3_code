import _ from 'lodash';

export const parseMeals = meals => {
  if (meals === null) {
    return null;
  }

  let filteredMeals = [];
  let currentTime = null;
  let indexCounter = -1;
  const addMealMoment = time => filteredMeals.push({ time: time, meals: [] });
  const addMeal = (index, meal) => filteredMeals[index].meals.push(meal);

  // Sort meals on meal moment
  _.forEach(meals, meal => {
    if (meal.time !== currentTime) {
      currentTime = meal.time;
      indexCounter++;

      addMealMoment(meal.time);
    }

    addMeal(indexCounter, meal);
  });

  return filteredMeals;
};

export const parseNextMeals = (state, nextMeals) => {
  let { meals } = state;
  const addNextMeals = index => meals[index].meals = _.concat(meals[index].meals, nextMeals);

  // Add next meals to existing meals with the same moment
  _.forEach(meals, (meal, index) => {
    if (meal.time === nextMeals[0].time) {
      addNextMeals(index);
    }
  });

  return meals;
};
