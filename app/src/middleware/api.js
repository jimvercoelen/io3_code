import fetch from 'isomorphic-fetch';

const base = 'http://localhost:8080/';

const getUrl = url => {
  return base + url;
};

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

const getBody = (method, data) => {
  if (method === 'GET') {
    return null;
  }

  return data ? JSON.stringify(data) : null;
};

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  let error = new Error(response.statusText);
  error.response = response;
  throw error;
};

const parseJSON = (response) => {
  // status 204 || 205 means a response without a body
  return (response.status === 204 || response.status === 205) ? null : response.json();
};

const dispatchSuccess = ({ dispatch, type, payload }) => {
  return dispatch({
    type: type + '_SUCCESS',
    payload
  });
};

const dispatchError = ({ dispatch, type, error }) => {
  return dispatch({
    type: type + '_ERROR',
    error: {
      error: error.message || error || 'Unknown',
      status: (error && error.response && error.response.status) || 0
    }
  });
};

const request = ({ url, method, data }, onSuccess, onError) => {
  url = getUrl(url);
  data = getBody(method, data);

  return fetch(url, {
    method: method,
    body: data,
    headers
  })
    .then(checkStatus)
    .then(parseJSON)
    .then(onSuccess)
    .catch(onError);
};

const api = store => dispatch => action => {
  if (!action.payload || !action.payload.url) {
    return dispatch(action);
  }

  const { type } = action;
  const { url, method, data } = action.payload;

  dispatch({
    ...action,
    type: type + '_PENDING'
  });

  return request({ url, method, data },
    payload => {
      return dispatchSuccess({ dispatch, type, payload });
    },
    error => {
      return dispatchError({ dispatch, type, error });
    });
};

export default api;
