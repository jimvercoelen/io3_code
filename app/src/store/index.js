let configStore;

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test') {
  configStore = require('./configureStore.prod');
} else {
  configStore = require('./configureStore.dev');
}

export default configStore();
