import { applyMiddleware, createStore, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import { browserHistory } from 'react-router';
import { routerMiddleware, routerReducer as routing } from 'react-router-redux';
import api from '../middleware/api';

import rootReducer from '../reducers';

const reducer = combineReducers({ ...rootReducer, routing });

export default (initialState = {}) => {
  return applyMiddleware(...[
    thunk,
    routerMiddleware(browserHistory),
    promiseMiddleware,
    api
  ])(createStore)(reducer, initialState);
};
