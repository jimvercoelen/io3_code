import { applyMiddleware, createStore, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import { browserHistory } from 'react-router';
import { routerReducer as routing, routerMiddleware } from 'react-router-redux';
import api from '../middleware/api';

import rootReducer from '../reducers';

const reducer = combineReducers({ ...rootReducer, routing });

const newStore = (initialState = {}) => {
  let middleware = applyMiddleware(...[
    thunk,
    routerMiddleware(browserHistory),
    promiseMiddleware,
    api
  ]);

  if (window.devToolsExtension) {
    middleware = compose(middleware, window.devToolsExtension());
  }

  const store = middleware(createStore)(reducer, initialState);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(require('../reducers'));
    });
  }

  return store;
};

export default newStore;
