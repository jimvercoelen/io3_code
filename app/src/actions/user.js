import { push } from 'react-router-redux';
import { setAuth, removeAuth } from '../helpers/localStorage';
import {
  USER_LOGIN,
  USER_CREATE_ACCOUNT,
  USER_UPDATE,
  USER_LOGOUT,
  USER_LOAD
} from '../constants/ActionTypes';
import { ROUTE_FOOD } from '../constants/Routes';

export const login = ({ email, password }) => dispatch => {
  return dispatch({
    type: USER_LOGIN,
    payload: {
      url: 'auth',
      method: 'POST',
      data: {
        email,
        password
      }
    }
  }).then(action => {
    if (action.type === (USER_LOGIN + '_SUCCESS')) {
      setAuth(action.payload.id);
    }
  });
};

export const loadUser = ({ id }) => dispatch => {
  return dispatch({
    type: USER_LOAD,
    payload: {
      url: `user/${id}`,
      method: 'GET'
    }
  });
};

export const createAccount = ({ email, password }) => dispatch => {
  return dispatch({
    type: USER_CREATE_ACCOUNT,
    payload: {
      url: 'user',
      method: 'POST',
      data: {
        email,
        password
      }
    }
  }).then(action => {
    if (action.type === (USER_CREATE_ACCOUNT + '_SUCCESS')) {
      setAuth(action.payload.id);
    }
  });
};

export const updateUser = data => dispatch => {
  return dispatch({
    type: USER_UPDATE,
    payload: {
      url: 'user',
      method: 'PATCH',
      data
    }
  }).then(action => {
    if (action.type === (USER_UPDATE + '_SUCCESS')) {
      dispatch(push(ROUTE_FOOD));
    }
  });
};

export const logout = () => dispatch => {
  removeAuth();

  return dispatch({
    type: USER_LOGOUT
  });
};
