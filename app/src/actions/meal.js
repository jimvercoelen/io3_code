import {
  MEALS_LOAD,
  MEALS_LOAD_NEXT,
  MEAL_LOAD_INGREDIENTS
} from '../constants/ActionTypes';

export const loadMeals = mealsADay => dispatch => {
  return dispatch({
    type: MEALS_LOAD,
    payload: {
      url: `meals/${mealsADay}`,
      method: 'GET'
    }
  });
};

export const loadNextMeals = meal => dispatch => {
  return dispatch({
    type: MEALS_LOAD_NEXT,
    payload: {
      url: `meals/${meal.time}/${meal.id}`,
      method: 'GET'
    }
  });
};

export const loadMealIngredients = meal => dispatch => {
  return dispatch({
    type: MEAL_LOAD_INGREDIENTS,
    payload: {
      url: `meal/ingredients/${meal.id}`,
      method: 'GET'
    }
  });
};
