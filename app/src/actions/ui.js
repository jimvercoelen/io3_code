import {
  UI_SHOW_SNACKBAR,
  UI_HIDE_SNACKBAR,
  UI_SHOW_MODAL,
  UI_HIDE_MODAL
} from '../constants/ActionTypes';

let isOpened = false;

export const showSnackbar = notification => dispatch => {
  if (!isOpened) {
    setTimeout(() => {
      dispatch(hideSnackbar());
      isOpened = false;
    }, 4000);
  }
  isOpened = true;

  return dispatch({
    type: UI_SHOW_SNACKBAR,
    payload: {
      notification
    }
  });
};

export const hideSnackbar = () => dispatch => {
  return dispatch({
    type: UI_HIDE_SNACKBAR
  });
};

export const showModal = data => dispatch => {
  return dispatch({
    type: UI_SHOW_MODAL,
    payload: data
  });
};

export const hideModal = () => dispatch => {
  return dispatch({
    type: UI_HIDE_MODAL
  });
};
