import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../actions/user';
import { showSnackbar } from '../actions/ui';
import { isFieldEmpty, isValidEmail } from '../helpers/validation';
import { ROUTE_CREATE_ACCOUNT } from '../constants/Routes';
import Card from '../components/Card/Card';
import Avatar from '../components/Avatar/Avatar';
import Form from '../components/Form/Form';
import FormGroup from '../components/FormGroup/FormGroup';
import Divider from '../components/Divider/Divider';
import Input from '../components/Input/Input';
import Button from '../components/Button/Button';

@connect(state => ({
  pending: state.user.login.pending,
  error: state.user.login.error
}), { login, showSnackbar })
export default class LoginForm extends Component {
  state = {
    fields: {
      email: {},
      password: {}
    }
  };

  handleError = nextProps => {
    const error = nextProps.error;

    if (error.status === 404) {
      nextProps.showSnackbar('Invalid email or password');
    } else if (error.status === 0) {
      nextProps.showSnackbar('Service is temporarily unavailable');
    } else {
      nextProps.showSnackbar('Oops, something went wrong');
    }
  };

  validate = callback => {
    const { fields } = this.state;
    const { email, password } = fields;
    let isValid = true;

    if (isFieldEmpty(email)) {
      email.error = 'Enter your email';
      isValid = false;
    } else if (!isValidEmail(email.value)) {
      email.error = 'Did you misspelled you email?';
      isValid = false;
    } else {
      email.error = null;
    }

    if (isFieldEmpty(password)) {
      password.error = 'Don`t forget your password';
      isValid = false;
    } else {
      password.error = null;
    }

    this.setState({
      email,
      password
    }, callback(isValid));
  };

  onInputValueChange = (event) => {
    const { name, value } = event.target;
    const { fields } = this.state;

    fields[name] = {
      ...fields[name],
      value
    };

    this.setState({ fields });
  };

  onFacebookClick = () => {
    console.log('facebook');
  };

  onGoogleClick = () => {
    console.log('google');
  };

  onSubmit = (e) => {
    e.preventDefault();

    this.validate(isValid => {
      if (isValid) {
        const { fields } = this.state;

        this.props.login({
          email: fields.email.value,
          password: fields.password.value
        });
      }
    });
  };

  get cardFooter () {
    return (
      <div className='card__footer'>
        <Button
          text='Password?'
          kind='flat'
          color='primary'/>
        <Button
          text='Create account'
          kind='flat'
          color='primary'
          to={ROUTE_CREATE_ACCOUNT} />
      </div>
    )
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.error) {
      this.handleError(nextProps);
    }
  }

  render () {
    const { pending } = this.props;
    const { fields } = this.state;
    const { email, password } = fields;

    return (
      <div>
        <Card
          title='Login'
          footer={this.cardFooter}>
          <Avatar />
          <Form
            pending={pending}
            onSubmit={this.onSubmit}>
            <FormGroup>
              <Button
                text='Facebook'
                color='facebook'
                size='half'
                onClick={this.onFacebookClick} />
              <Button
                text='Google'
                color='google'
                size='half'
                onClick={this.onGoogleClick} />
            </FormGroup>
            <FormGroup>
              <Divider text='OR' />
            </FormGroup>
            <FormGroup>
              <Input
                name='email'
                type='email'
                value={email}
                onChange={this.onInputValueChange} />
            </FormGroup>
            <FormGroup>
              <Input
                name='password'
                type='password'
                value={password}
                onChange={this.onInputValueChange} />
            </FormGroup>
            <FormGroup
              position='right'>
              <Button
                text='Login'
                type='submit'
                kind='flat'
                color='primary' />
            </FormGroup>
          </Form>
        </Card>
      </div>
    );
  }
}
