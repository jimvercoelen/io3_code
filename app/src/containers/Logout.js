import { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../actions/user';
import { withRouter } from 'react-router';

@withRouter
@connect(null, { logout })
export default class Logout extends Component {
  componentWillMount () {
    this.props.logout();
  }

  render () {
    return null;
  }
}
