import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import createRoutes from '../../routes';

export default class Root extends Component {
  render () {
    const { store, history } = this.props;
    const routes = createRoutes({ store });

    return (
      <Provider store={store}>
        <Router history={history} key={Math.random()} routes={routes} />
      </Provider>
    );
  }
}
