import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import createRoutes from '../../routes';

export default ({ store, history }) => (
  <Provider store={store}>
    <Router history={history} routes={createRoutes({ store })} />
  </Provider>
);
