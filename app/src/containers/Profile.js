import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { updateUser } from '../actions/user';
import { showSnackbar } from '../actions/ui';
import { isFieldEmpty } from '../helpers/validation';
import Card from '../components/Card/Card';
import Form from '../components/Form/Form';
import FormGroup from '../components/FormGroup/FormGroup';
import Range from '../components/Range/Range';
import SelectSingle from '../components/Select/SelectSingle';
import SelectMultiple from '../components/Select/SelectMultiple';
import Button from '../components/Button/Button';
import Calories from '../components/Calories/Calories';

@connect(state => ({
  pending: state.user.updateProfile.pending,
  error: state.user.updateProfile.error,
  auth: state.user.auth
}), { updateUser, showSnackbar })
export default class ProfileForm extends Component {
  state = {
    need: null,
    fields: {
      gender: {
        value: {}
      },
      activity: {
        value: {}
      },
      goal: {
        value: {}
      },
      allergies: {
        value: []
      },
      age: {},
      height: {},
      weight: {},
      mealsADay: {}
    }
  };

  genderOptions = [
    { text: 'Male', value: 'm' },
    { text: 'Female', value: 'f' }
  ];

  activityOptions = [
    { text: 'Inactive', value: 1.2 },
    { text: 'Light Active', value: 1.375 },
    { text: 'Average Active', value: 1.55 },
    { text: 'Very Active', value: 1.725 },
    { text: 'Extremely Active', value: 1.9 }
  ];

  goalOptions = [
    { text: 'Lose Weight', value: 'l' },
    { text: 'Keep Weight', value: 'k' },
    { text: 'Gain Weight', value: 'g' }
  ];

  allergiesOptions = [
    { text: 'Option 1', value: '1' },
    { text: 'Option 2', value: '2' },
    { text: 'Option 3', value: '3' },
    { text: 'Option 4', value: '4' },
    { text: 'Option 5', value: '5' },
    { text: 'Option 6', value: '6' },
    { text: 'Option 7', value: '7' },
    { text: 'Option 8', value: '8' }
  ];

  calculateNeedOfCalories = ({ fields }) => {
    const { goal, gender, activity, age, height, weight } = fields;

    if (!gender.value.value || !activity.value.value || !goal.value.value) {
      return;
    }

    let need;
    let goalOffset;
    let ageFactor;
    let heightFactor;
    let weightFactor;

    // Set factors depending on the gender
    if (gender.value.value === 'm') {
      need = 88.362;
      ageFactor = 5.677;
      heightFactor = 4.799;
      weightFactor = 13.397;
    } else {
      need = 447.593;
      ageFactor = 4.33;
      heightFactor = 3.098;
      weightFactor = 9.247;
    }

    // Set goal offset depending on goal
    if (goal.value.value === 'l') {
      goalOffset = -150;
    } else if (goal.value.value === 'g') {
      goalOffset = 150;
    } else {
      goalOffset = 0;
    }

    // Calculate need of calories
    need += (weightFactor * weight.value);
    need += (heightFactor * height.value);
    need -= (ageFactor * age.value);
    need *= activity.value.value ? activity.value.value : 1.55;
    need += goalOffset;
    need = need.toFixed(0);

    return need;
  };

  handleError = nextProps => {
    const error = nextProps.error;

    if (error.status === 0) {
      nextProps.showSnackbar('Service is temporarily unavailable');
    } else {
      nextProps.showSnackbar('Oops, something went wrong');
    }
  };

  filterState = () => {
    let data = _.assign({}, this.state.fields);
    const filterObject = obj => obj.value;
    const filterArray = array => _.map(array, obj => obj.value);

    data = {
      id: this.props.auth.id,
      email: this.props.auth.email,
      goal: filterObject(data.goal.value),
      gender: filterObject(data.gender.value),
      activity: filterObject(data.activity.value),
      allergies: filterArray(data.allergies.value),
      age: parseInt(filterObject(data.age)),
      height: parseInt(filterObject(data.height)),
      weight: parseInt(filterObject(data.weight)),
      mealsADay: parseInt(filterObject(data.mealsADay)),
      need: parseInt(this.state.need)
    };

    return data;
  };

  validate = callback => {
    const { fields } = this.state;
    const { goal, gender, activity } = fields;
    let isValid = true;

    if (isFieldEmpty(goal.value)) {
      goal.error = 'What is your goal?';
      isValid = false;
    } else {
      goal.error = null;
    }

    if (isFieldEmpty(gender.value)) {
      gender.error = 'What is your gender?';
      isValid = false;
    } else {
      gender.error = null;
    }

    if (isFieldEmpty(activity.value)) {
      activity.error = 'How active are you?';
      isValid = false;
    } else {
      activity.error = null;
    }

    this.setState({
      goal,
      gender,
      activity
    }, callback(isValid));
  };

  onInputValueChange = (name, value) => {
    const { fields } = this.state;

    fields[name] = {
      ...fields[name],
      value
    };

    this.setState({
      fields,
      need: this.calculateNeedOfCalories(this.state)
    });
  };

  onSubmit = (e) => {
    e.preventDefault();

    this.validate(isValid => {
      if (isValid) {
        this.props.updateUser(this.filterState());
      }
    });
  };

  componentWillReceiveProps (nextProps) {
    if (nextProps.error) {
      this.handleError(nextProps);
    }
  }

  render () {
    const { auth, pending } = this.props;
    const { fields, need } = this.state;
    const { gender, activity, goal, allergies } = fields;

    const genderInitialValue = _.find(this.genderOptions, obj => obj.value === auth.gender);
    const activityInitialValue = _.find(this.activityOptions, obj => obj.value === auth.activity);
    const goalInitialValue = _.find(this.goalOptions, obj => obj.value === auth.goal);
    const allergiesInitialValue = _.filter(this.allergiesOptions, obj =>
      auth.allergies && auth.allergies.indexOf(obj.value) !== -1);

    return (
      <Card title='Profile'>
        <Form
          pending={pending}
          onSubmit={this.onSubmit}>
          <FormGroup>
            <SelectSingle
              name='gender'
              placeholder='Gender'
              options={this.genderOptions}
              initialValue={genderInitialValue}
              value={gender}
              onChange={this.onInputValueChange} />
          </FormGroup>
          <FormGroup>
            <SelectSingle
              name='activity'
              placeholder='Activity'
              options={this.activityOptions}
              initialValue={activityInitialValue}
              value={activity}
              onChange={this.onInputValueChange} />
          </FormGroup>
          <FormGroup>
            <SelectSingle
              name='goal'
              placeholder='Goal'
              options={this.goalOptions}
              initialValue={goalInitialValue}
              value={goal}
              onChange={this.onInputValueChange} />
          </FormGroup>
          <FormGroup>
            <SelectMultiple
              name='allergies'
              placeholder='Allergies'
              options={this.allergiesOptions}
              initialValue={allergiesInitialValue}
              value={allergies}
              onChange={this.onInputValueChange} />
          </FormGroup>
          <FormGroup space='large'>
            <Range
              name='age'
              label='Age'
              min={16}
              max={100}
              defaultValue={auth.age ? auth.age : 23}
              onChange={_.throttle(this.onInputValueChange, 500, { leading: false })} />
          </FormGroup>
          <FormGroup space='large'>
            <Range
              name='weight'
              label='Weight'
              labelEnd='(kg)'
              min={40}
              max={200}
              defaultValue={auth.weight ? auth.weight : 75}
              onChange={_.throttle(this.onInputValueChange, 500, { leading: false })} />
          </FormGroup>
          <FormGroup space='large'>
            <Range
              name='height'
              label='Height'
              labelEnd='(cm)'
              min={140}
              max={240}
              defaultValue={auth.height ? auth.height : 175}
              onChange={_.throttle(this.onInputValueChange, 500, { leading: false })} />
          </FormGroup>
          <FormGroup space='large'>
            <Range
              name='mealsADay'
              label='Meals a day'
              min={3}
              max={8}
              defaultValue={auth.mealsADay ? auth.mealsADay : 6}
              onChange={_.throttle(this.onInputValueChange, 500, { leading: false })} />
          </FormGroup>
          <Calories need={need} />
          <FormGroup position='right'>
            <Button
              text='Next'
              type='submit'
              kind='flat'
              color='primary' />
          </FormGroup>
        </Form>
      </Card>
    );
  }
}
