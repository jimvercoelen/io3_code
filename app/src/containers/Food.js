import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadMeals, loadNextMeals, loadMealIngredients } from '../actions/meal';
import { showModal } from '../actions/ui';
import _ from 'lodash';
import ReactSwipe from 'react-swipe';
import Card from '../components/Card/Card';
import Meal from '../components/Meal/Meal';
import Modal from '../components/Modal/Modal';
import Ingredients from '../components/Ingredients/Ingredients';

@connect(state => ({
  auth: state.user.auth,
  meals: state.meal.meals,
  ingredients: state.meal.ingredients,
  modal: state.ui.modal,
  error: state.meal.load.error || state.meal.loadNext.error || state.meal.loadIngredients.error
}), { loadMeals, loadNextMeals, loadMealIngredients, showModal })
export default class Food extends Component {
  swipeOptions = {
    startSlide: 0,
    auto: 0,
    speed: 300,
    disableScroll: false,
    continuous: false,
    callback () {
      // Empty (slide changed)
    },
    transitionEnd () {
      // Empty (ended transition)
    }
  };

  state = {
    last: null
  };

  onPreviousClick = rowReference => e => {
    e.stopPropagation();

    this.refs[rowReference].prev();
  };

  onNextClick = (rowReference, meal) => e => {
    e.stopPropagation();

    const { loadNextMeals } = this.props;
    let reactSlider = this.refs[rowReference];
    const position = reactSlider.getPos();
    const amount = reactSlider.getNumSlides();

    // Load next meals 1meal before the last one.
    if ((position + 1) === amount) {
      loadNextMeals(meal).then(() => {

        // Hacky solution but with the use of the timeout
        // the next meal is nicely animated after being fetched.
        this.setState({
          last: {
            time: meal.time,
            position
          }
        }, () => setTimeout(() => {
            reactSlider = this.refs[rowReference];
            reactSlider.next();
          }, 1));
      });

      return;
    }

    reactSlider.next();
  };

  onMealClick = meal => () => {
    const { loadMealIngredients, showModal } = this.props;

    loadMealIngredients(meal).then(() => {
      showModal(meal);
    });
  };

  renderMeals = (meals, rowIndex, rowReference) =>
    _.times(meals.length, index => {
      const meal = meals[index];

      return (
        <div key={index}>
          <Meal
            meal={meal}
            onPreviousClick={this.onPreviousClick(rowReference)}
            onNextClick={this.onNextClick(rowReference, meal)}
            onMealClick={this.onMealClick(meal)} />
        </div>
      );
    });

  renderMealRow = (mealMoment, rowIndex) => {
    const rowReference = 'row' + rowIndex;
    const { last } = this.state;
    let swipeOptions = this.swipeOptions;

    if (typeof mealMoment === 'undefined') {
      return null;
    }

    // If last use that position as last position
    if (last && last.time === mealMoment.time) {
      swipeOptions.startSlide = last.position;
    }

    return (
      <div key={rowIndex}>
        <p style={{
          margin: '0 .8rem .4rem',
          fontWeight: '500'
        }}>
          {mealMoment.time}
        </p>
        <ReactSwipe
          key={mealMoment.meals.length}
          ref={rowReference}
          className='meal__row'
          swipeOptions={swipeOptions}>
          {this.renderMeals(mealMoment.meals, rowIndex, rowReference)}
        </ReactSwipe>
      </div>
    );
  };

  get modalContent () {
    const { ingredients, modal } = this.props;
    const meal = modal.data;

    return (
      <div>
        <p>
          <span style={{ fontWeight: 500 }}>
            Description
          </span>
          <br />
          {meal.description}
        </p>
        <Ingredients ingredients={ingredients} />
      </div>
    )
  }

  componentWillMount () {
    this.props.loadMeals(this.props.auth.mealsADay);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.error) {
      console.log(nextProps.error);
      // TODO: handle error
    }
  }

  render () {
    const { meals, auth, modal } = this.props;
    const mealRows = meals ? _.times(auth.mealsADay, index => this.renderMealRow(meals[index], index)) : null;
    const mealModal = modal.show ? (
        <Modal title={modal.data.name}>
          {this.modalContent}
        </Modal>
      ) : null;

    return (
      <Card title='Food' contentStyle={{ margin: '0' }}>
        {mealRows}
        {mealModal}
      </Card>
    );
  }
}
