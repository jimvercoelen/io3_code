import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createAccount } from '../actions/user';
import { showSnackbar } from '../actions/ui';
import { isFieldEmpty, isValidEmail, isValidPassword } from '../helpers/validation';
import { ROUTE_LOGIN } from '../constants/Routes';
import Card from '../components/Card/Card';
import Avatar from '../components/Avatar/Avatar';
import Form from '../components/Form/Form';
import FormGroup from '../components/FormGroup/FormGroup';
import Input from '../components/Input/Input';
import Button from '../components/Button/Button';

@connect(state => ({
  error: state.user.createAccount.error,
  pending: state.user.createAccount.pending
}), { createAccount, showSnackbar })
export default class LoginForm extends Component {
  state = {
    fields: {
      email: {},
      password: {}
    }
  };

  handleError = nextProps => {
    const error = nextProps.error;

    if (error.status === 404) {
      nextProps.showSnackbar('Email address already taken');
    } else if (error.status === 0) {
      nextProps.showSnackbar('Service is temporarily unavailable');
    } else {
      nextProps.showSnackbar('Oops, something went wrong');
    }
  };

  validate = (callback) => {
    const { fields } = this.state;
    const { email, password } = fields;
    let isValid = true;

    if (isFieldEmpty(email)) {
      email.error = 'Enter your email';
      isValid = false;
    } else if (!isValidEmail(email.value)) {
      email.error = 'Did you misspelled you email?';
      isValid = false;
    } else {
      email.error = null;
    }

    if (isFieldEmpty(password)) {
      password.error = 'Enter a password';
      isValid = false;
    } else if (!isValidPassword(password.value)) {
      password.error = 'Make sure it`s at least 6 characters long';
      isValid = false;
    } else {
      password.error = null;
    }

    this.setState({
      email,
      password
    }, callback(isValid));
  };

  onInputValueChange = (event) => {
    const { name, value } = event.target;
    const { fields } = this.state;

    fields[name] = {
      ...fields[name],
      value
    };

    this.setState({ fields });
  };

  onSubmit = (e) => {
    e.preventDefault();

    this.validate((isValid) => {
      if (isValid) {
        const { fields } = this.state;

        this.props.createAccount({
          email: fields.email.value,
          password: fields.password.value
        });
      }
    });
  };

  get cardFooter () {
    return (
      <div className='card__footer'>
        <Button
          text='Login'
          kind='flat'
          color='primary'
          to={ROUTE_LOGIN} />
      </div>
    )
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.error) {
      this.handleError(nextProps);
    }
  }

  render () {
    const { pending } = this.props;
    const { fields } = this.state;
    const { email, password } = fields;

    return (
      <div>
        <Card
          title='Create account'
          footer={this.cardFooter}>
          <Avatar />
          <Form
            pending={pending}
            onSubmit={this.onSubmit}>
            <FormGroup>
              <Input
                name='email'
                type='email'
                value={email}
                onChange={this.onInputValueChange} />
            </FormGroup>
            <FormGroup>
              <Input
                name='password'
                type='text'
                value={password}
                onChange={this.onInputValueChange} />
            </FormGroup>
            <FormGroup
              position='right'>
              <Button
                text='Create'
                kind='flat'
                type='submit'
                color='primary' />
            </FormGroup>
          </Form>
        </Card>
      </div>
    );
  }
}
