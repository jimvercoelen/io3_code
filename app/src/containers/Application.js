import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isLoggedIn, isProfileSet } from '../helpers/state';
import Header from '../components/Header/Header';
import { withRouter } from 'react-router';
import Content from '../components/Content/Content';
import Snackbar from '../components/Snackbar/Snackbar';

import {
  ROUTE_LOGIN,
  ROUTE_PROFILE,
  ROUTE_FOOD
} from '../constants/Routes';

@withRouter
@connect(state => ({
  isLoggedIn: isLoggedIn(state),
  isProfileSet: isProfileSet(state)
}))
export default class App extends Component {
  componentWillReceiveProps (nextProps) {
    const redirect = location => this.props.router.push(location);
    const isLoggedIn = !this.props.isLoggedIn && nextProps.isLoggedIn;
    const isLoggedOut = this.props.isLoggedIn && !nextProps.isLoggedIn;

    if (isLoggedIn) {
      if (nextProps.isProfileSet) {
        redirect(ROUTE_FOOD);
      } else {
        redirect(ROUTE_PROFILE);
      }
    }

    if (isLoggedOut) {
      redirect(ROUTE_LOGIN);
    }
  };

  render () {
    const { children, isLoggedIn, isProfileSet } = this.props;

    return (
      <div>
        <div>
          <Header
            isLoggedIn={isLoggedIn}
            isProfileSet={isProfileSet} />
          <Content>
            {children}
          </Content>
        </div>
        <Snackbar />
      </div>
    );
  }
}
