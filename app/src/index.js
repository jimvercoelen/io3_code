import React from 'react';
import { render } from 'react-dom';
import Root from './containers/Root';
import { browserHistory } from 'react-router';
import { AppContainer } from 'react-hot-loader';
import { syncHistoryWithStore } from 'react-router-redux';
import store from './store';
import './index.scss';

function renderApp (RootComponent) {
  render(
    <AppContainer>
      <RootComponent
        store={store}
        history={syncHistoryWithStore(browserHistory, store)} />
    </AppContainer>,
    document.getElementById('root')
  );
}

renderApp(Root);

if (__DEV__) {
  if (module.hot) {
    module.hot.accept(
      './containers/Root',
      () => renderApp(require('./containers/Root'))
    );
  }
}
