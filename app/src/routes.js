import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { isLoggedIn, isProfileSet } from './helpers/state';
import { showSnackbar } from './actions/ui';
import { getAuth, removeAuth } from './helpers/localStorage';
import { loadUser } from './actions/user';
import Logout from './containers/Logout';
import App from './containers/Application';
import Login from './containers/Login';
import CreateAccount from './containers/CreateAccount';
import Profile from './containers/Profile';
import Food from './containers/Food';

import {
  ROUTE_HOME,
  ROUTE_LOGIN,
  ROUTE_CREATE_ACCOUNT,
  ROUTE_PROFILE,
  ROUTE_FOOD,
  ROUTE_LOGOUT
} from './constants/Routes';

export default function createRoutes ({ store }) {
  const requireLogin = (nextState, replace, done) => {
    if (!isLoggedIn(store.getState())) {
      replace(ROUTE_LOGIN);
    }

    done();
  };

  const initializeAuth = (nextState, replace, done) => {
    const auth = getAuth();

    if (auth) {
      store.dispatch(loadUser({ id: auth })).then((action) => {
        if (action.type === 'USER_LOAD_ERROR') {
          removeAuth();
          if (error.status === 0) {
            store.dispatch(showSnackbar('Service is temporarily unavailable'));
          } else {
            store.dispatch(showSnackbar('Oops, something went wrong'));
          }
        }
      });
    }

    done();
  };

  const getIndexRoute = (nextState, replace, done) => {
    const state = store.getState();

    if (isLoggedIn(state)) {
      if (isProfileSet(state)) {
        replace(ROUTE_FOOD);
      } else {
        replace(ROUTE_PROFILE);
      }
    } else {
      replace(ROUTE_LOGIN);
    }

    console.log('get index route');

    done();
  };

  return (
    <Route path={ROUTE_HOME} onEnter={initializeAuth} component={App}>
      <IndexRoute onEnter={getIndexRoute} />
      <Route path={ROUTE_LOGIN} component={Login} />
      <Route path={ROUTE_CREATE_ACCOUNT} component={CreateAccount} />
      <Route onEnter={requireLogin}>
        <Route path={ROUTE_PROFILE} component={Profile} />
        <Route path={ROUTE_FOOD} component={Food} />
        <Route path={ROUTE_LOGOUT} component={Logout} />
      </Route>
    </Route>
  );
}
