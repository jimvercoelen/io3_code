import React, { PropTypes } from 'react';
import './Card.scss';

const Card = ({ children, title, footer, contentStyle }) => (
  <div className='card'>
    <div className='card__header'>
      {title}
    </div>
    <div
      className='card__content'
      style={contentStyle}>
      {children}
    </div>
    {footer}
  </div>
);

Card.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string.isRequired,
  contentStyle: PropTypes.object,
  footer: PropTypes.node
};

export default Card;
