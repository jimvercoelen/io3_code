import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import './Input.scss';

export default class Input extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.object,
    type: PropTypes.oneOf([
      'text',
      'email',
      'password'
    ]).isRequired,
    onChange: PropTypes.func.isRequired,
    title: PropTypes.string
  };

  state = {
    isFocussed: false
  };

  onFocus = () => {
    this.setState({ isFocussed: true });
  };

  onBlur = () => {
    this.setState({ isFocussed: false });
  };

  get placeholder () {
    const { name } = this.props;
    return name.charAt(0).toUpperCase() + name.slice(1);
  }

  get error () {
    const { value } = this.props;

    return value && value.error ? (
      <div className='input__error'>
        {value.error}
      </div>
    ) : null;
  }

  render () {
    const { type, name, title, value, onChange } = this.props;
    const { isFocussed } = this.state;

    return (
      <div
        className={cx({
          'input': true,
          'input--state-focussed': isFocussed,
          'input--state-fixed': value && value.value
        })}>
        <div className='input__label'>
          {this.placeholder}
        </div>
        <input
          className='input__control'
          autoComplete='off'
          type={type}
          name={name}
          title={title}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onChange={onChange} />
        {this.error}
      </div>
    );
  }
}
