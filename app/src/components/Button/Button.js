import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import cx from 'classnames';
import Ink from 'react-ink';
import './Button.scss';

export default class Button extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    kind: PropTypes.oneOf([
      'raised',
      'flat'
    ]).isRequired,
    color: PropTypes.oneOf([
      'default',
      'primary',
      'google',
      'facebook'
    ]).isRequired,
    size: PropTypes.oneOf([
      'crop',
      'half',
      'full'
    ]).isRequired,
    type: PropTypes.oneOf([
      'button',
      'submit',
      'reset'
    ]).isRequired,
    to: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string
    ]).isRequired,
    onClick: PropTypes.func
  };

  static defaultProps = {
    kind: 'raised',
    color: 'default',
    size: 'crop',
    type: 'button',
    to: false
  };

  state = {
    active: false
  };

  render () {
    const { text, kind, color, size, position, type, to, onClick } = this.props;

    if (to) {
      return (
        <Link
          className={cx({
            'button': true,
            [`button--kind-${kind}`]: kind,
            [`button--color-${color}`]: color,
            [`button--size-${size}`]: size,
            [`button--position-${position}`]: position
          })}
          to={to}>
          {text}
          <Ink />
        </Link>
      );
    }

    return (
      <button
        className={cx({
          'button': true,
          [`button--kind-${kind}`]: kind,
          [`button--color-${color}`]: color,
          [`button--size-${size}`]: size,
          [`button--position-${position}`]: position
        })}
        type={type}
        onClick={onClick}>
        {text}
        <Ink />
      </button>
    );
  }
};
