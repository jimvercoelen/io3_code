import React, { PropTypes } from 'react';
import './Content.scss';

const Content = ({ children }) => (
  <div className='content'>
    <div className='content__inner'>
      {children}
    </div>
  </div>
);

Content.propTypes = {
  children: PropTypes.node.isRequired
};

export default Content;
