import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import AnimatedNumber from 'react-animated-number';
import './Calories.scss';

const getAnimatedNumber = need => {
  if (!need || isNaN(need)) {
    return null;
  }

  return (
    <AnimatedNumber
      className='calories__animated-number'
      stepPrecision={0}
      frameStyle={perc => (perc === 100 || isNaN(perc) ? {} : {opacity: 0.25})}
      duration={300}
      value={parseInt(need)} />
  );
};

const Calories = ({ need }) => (
  <p className='calories'>
    Your daily need of calories
    <ReactCSSTransitionGroup
      transitionName='calories__transition-group'
      transitionEnterTimeout={500}
      transitionLeaveTimeout={300}>
      {getAnimatedNumber(need)}
    </ReactCSSTransitionGroup>
  </p>
);

export default Calories;

