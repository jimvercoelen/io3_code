import React, { PropTypes } from 'react';
import './Form.scss';

const Form = ({ children, onSubmit }) => (
  <form
    className='form'
    autoComplete='off'
    noValidate
    onSubmit={onSubmit}>
    {children}
  </form>
);

Form.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default Form;
