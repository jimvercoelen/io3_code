import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import './Ingredients.scss';

export default class Ingredients extends Component {
  static PropTypes = {
    ingredients: PropTypes.array.isRequired
  };

  get ingredients () {
    const { ingredients } = this.props;
    console.log(ingredients);

    return _.map(ingredients, (ingredient, index) =>
      <div
        key={index}
        className='ingredients__row'>
        <div className='ingredients__cell'>
          {ingredient.productDescription}
        </div>
        <div className='ingredients__cell'>
          {ingredient.kcal}
        </div>
        <div className='ingredients__cell'>
          {ingredient.kj}
        </div>
        <div className='ingredients__cell'>
          {ingredient.water}
        </div>
        <div className='ingredients__cell'>
          {ingredient.protein}
        </div>
        <div className='ingredients__cell'>
          {ingredient.carbohydrates}
        </div>
        <div className='ingredients__cell'>
          {ingredient.sugars}
        </div>
        <div className='ingredients__cell'>
          {ingredient.fat}
        </div>
        <div className='ingredients__cell'>
          {ingredient.monounsaturated}
        </div>
        <div className='ingredients__cell'>
          {ingredient.polyunsaturated}
        </div>
        <div className='ingredients__cell'>
          {ingredient.cholesterol}
        </div>
        <div className='ingredients__cell'>
          {ingredient.fibers}
        </div>
      </div>
    )
  }

  get header () {
    return (
      <div>
        <div className='ingredients__row'>
          <div className='ingredients__cell'>
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Energy
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Energy
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Water
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Protein
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Carbs
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Sugar
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Fat
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            m.u.f.a
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            p.u.f.a
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Chol
          </div>
          <div className='ingredients__cell ingredients__cell--rotated'>
            Fiber
          </div>
        </div>
        <div className='ingredients__row'>
          <div className='ingredients__cell'>
            100 g serving
          </div>
          <div className='ingredients__cell'>
            Kcal
          </div>
          <div className='ingredients__cell'>
            Kj
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            g
          </div>
          <div className='ingredients__cell'>
            mg
          </div>
          <div className='ingredients__cell'>
            g
          </div>
        </div>
      </div>
    )
  }

  render () {
    return (
      <div className='ingredients'>
        {this.header}
        {this.ingredients}
      </div>
    )
  }
}
