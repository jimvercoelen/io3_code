import React, { PropTypes } from 'react';
import cx from 'classnames';
import './FormGroup.scss';

const FormGroup = ({ children, position, space }) => (
  <div
    className={cx({
      'form__group': true,
      [`form__group--position-${position}`]: position,
      [`form__group--space-${space}`]: space
    })}>
    {children}
  </div>
);

FormGroup.propTypes = {
  children: PropTypes.node.isRequired,
  position: PropTypes.oneOf([
    'left',
    'right',
    false
  ]).isRequired
};

FormGroup.defaultProps = {
  position: false,
  space: 'medium'
};

export default FormGroup;
