import React from 'react';
import image from './Avatar.jpg';
import './Avatar.scss';

const Avatar = () => (
  <div className='avatar'>
    <img
      className='avatar__image'
      src={image} />
  </div>
);

export default Avatar;
