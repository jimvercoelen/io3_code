import React, { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Ink from 'react-ink';
import _ from 'lodash';
import './Dropdown.scss';

export default class Dropdown extends Component {
  static propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    items: PropTypes.array.isRequired
  };

  state = {
    isOpen: false
  };

  expand = () => {
    this.setState({ isOpen: true });
  };

  collapse = () => {
    this.setState({ isOpen: false });
  };

  get items () {
    const { items } = this.props;

    return _.map(items, (item, index) =>
      <li
        key={index}
        className='dropdown__item'
        onClick={item.onClick}>
        <i className='dropdown__item__icon material-icons'>
          {item.icon}
        </i>
        {item.text}
        <Ink opacit={0.1} />
      </li>
    );
  }

  get drop () {
    return this.state.isOpen ? (
      <ul
        className='dropdown__drop'
        onClick={this.collapse}>
        {this.items}
      </ul>
    ) : null;
  }

  get trigger () {
    return this.props.isLoggedIn ? (
      <i
        className='dropdown__trigger material-icons'
        onClick={this.expand}>
        more_horiz
      </i>
    ) : null;
  }

  render () {
    return (
      <div className='dropdown' tabIndex='0' onBlur={this.collapse}>
        {this.trigger}
        <ReactCSSTransitionGroup
          transitionName='dropdown__transition-group'
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {this.drop}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}
