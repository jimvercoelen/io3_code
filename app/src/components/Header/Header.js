import React, { Component, PropTypes } from 'react';
import { withRouter } from 'react-router';
import { ROUTE_LOGOUT, ROUTE_PROFILE, ROUTE_FOOD } from '../../constants/Routes';
import Dropdown from '../Dropdown/Dropdown.js';
import './Header.scss';

@withRouter
export default class Header extends Component {
  static propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    isProfileSet: PropTypes.bool.isRequired
  };

  onProfileClick = () => {
    this.props.router.push(ROUTE_PROFILE);
  };

  onRefreshClick = () => {
    console.log('refresh click');
  };

  onSignOutClick = () => {
    this.props.router.push(ROUTE_LOGOUT);
  };

  get dropdown () {
    const { router, isLoggedIn } = this.props;
    const currentRouterPath = router.location.pathname;

    // Only the profile and food routes should have a drop down
    if (currentRouterPath !== ROUTE_PROFILE && currentRouterPath !== ROUTE_FOOD) {
      return null;
    }

    // Set items depending on the route
    let dropdownItems = [
      { text: 'Sign out', icon: 'exit_to_app', onClick: this.onSignOutClick }
    ];

    // the drop down should only contain the following
    // menu items when the current route is the food route.
    if (currentRouterPath === ROUTE_FOOD) {
      dropdownItems.push(
        { text: 'Profile', icon: 'person', onClick: this.onProfileClick },
        { text: 'Refresh', icon: 'refresh', onClick: this.onRefreshClick }
      );
      // dropdownItems.push({ text: 'Refresh', icon: 'refresh', onClick: this.onRefreshClick });
    }

    return (
      <Dropdown
        isLoggedIn={isLoggedIn}
        items={dropdownItems} />
    );
  }

  render () {
    return (
      <div className='header'>
        <div className='header__title'>
          Shaping
        </div>
        {this.dropdown}
      </div>
    );
  }
}
