import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { hideModal } from '../../actions/ui';
import cx from 'classnames';
import './Modal.scss';

@connect(state => ({
  modal: state.ui.modal
}), { hideModal })
export default class Modal extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
  };

  onCloseClick = () => {
    this.props.hideModal();
  };

  listenKeyBoard = e => {
    if (e.key === 'Escape' || e.keyCode == 27) {
      this.props.hideModal();
    }
  };

  componentDidMount () {
    window.addEventListener('keydown', this.listenKeyBoard);
  }

  componentWillUnmount () {
    window.removeEventListener('keydown', this.listenKeyBoard);
  }

  render () {
    const { title, children } = this.props;

    return (
      <div className={cx({
        'modal': true,
        'modal--show': true
      })}>
        <div className='modal__overlay'>
          <div className='modal__dialog'>
            <div className='modal__header'>
              <i
                className='modal__header__close material-icons'
                onClick={this.onCloseClick}>
                clear
              </i>
              { title }
            </div>
            <div className='modal__content'>
              { children }
            </div>
          </div>
        </div>
      </div>
    )
  }
}
