import React, { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Ink from 'react-ink';
import cx from 'classnames';
import _ from 'lodash';
import './Select.scss';

export default class SelectMultiple extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
  };

  state = {
    isOpen: false
  };

  onOptionItemMouseDown = option => () => {
    this.changeValue(option);
  };

  onPageClick = e => {
    e.preventDefault();

    if (e.target.className !== 'ink') {
      window.removeEventListener('mousedown', this.onPageClick);
      this.setState({ isOpen: false });
    }
  };

  onPlaceholderClick = () => {
    window.addEventListener('mousedown', this.onPageClick);
    this.setState({ isOpen: true });
  };

  changeValue = newValue => {
    const { name, onChange, value } = this.props;

    const containsOption = _.some(value.value, newValue);
    if (containsOption) {
      onChange(name, _.filter(value.value, (value) => value.value !== newValue.value));
    } else {
      onChange(name, _.concat(newValue, value.value));
    }
  };

  get checkbox () {
    return (
      <div className='select__checkbox'>
        <i className='select__checkbox__icon material-icons'>
          done
        </i>
      </div>
    );
  }

  get options () {
    const { isOpen } = this.state;
    const { options, value } = this.props;

    if (!isOpen) {
      return null;
    }

    const isOptionSelected = (option) => {
      const containsOption = _.some(value.value, option);
      return containsOption || (value.value.value === option.value);
    };

    return (
      <ul className='select__options'>
        {_.map(options, (option, index) =>
          <li
            className={cx({
              'select__option': true,
              'select__option--selected': isOptionSelected(option)
            })}
            key={index}
            onMouseDown={this.onOptionItemMouseDown(option)}>
            {this.checkbox}
            {option.text}
            <Ink opacit={0.1} />
          </li>
        )}
      </ul>
    );
  }

  get placeholder () {
    const { value, placeholder } = this.props;
    let text = '';

    _.each(value.value, value => text += value.text + ', ');

    return text === '' ? placeholder : text;
  }

  get error () {
    const { value } = this.props;

    return value && value.error ? (
      <div className='select__error'>
        {value.error}
      </div>
    ) : null;
  }

  componentDidMount () {
    const { initialValue } = this.props;
    if (!initialValue) {
      return;
    }

    this.changeValue(initialValue);
  }

  render () {
    return (
      <div className='select'>
        <i className='select__icon material-icons'>
          arrow_drop_down
        </i>
        <div
          onClick={this.onPlaceholderClick}
          className='select__placeholder'>
          {this.placeholder}
        </div>
        {this.error}
        <ReactCSSTransitionGroup
          transitionName='select__transition-group'
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {this.options}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}
