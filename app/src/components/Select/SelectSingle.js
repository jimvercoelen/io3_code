import React, { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Ink from 'react-ink';
import cx from 'classnames';
import _ from 'lodash';
import './Select.scss';

export default class Select extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
  };

  state = {
    isOpen: false
  };

  onOptionItemMouseDown = (option) => () => {
    this.changeValue(option);
  };

  onPageClick = (e) => {
    e.preventDefault();

    window.removeEventListener('mousedown', this.onPageClick);
    this.setState({ isOpen: false });
  };

  onPlaceholderClick = () => {
    window.addEventListener('mousedown', this.onPageClick);
    this.setState({ isOpen: true });
  };

  changeValue = newValue => {
    const { name, onChange } = this.props;

    onChange(name, newValue);
  };

  get options () {
    const { isOpen } = this.state;
    const { options, value } = this.props;

    if (!isOpen) {
      return null;
    }

    const isOptionSelected = (option) => {
      return value.value.value === option.value;
    };

    return (
      <ul className='select__options'>
        {_.map(options, (option, index) =>
          <li
            className={cx({
              'select__option': true,
              'select__option--selected': isOptionSelected(option)
            })}
            key={index}
            onMouseDown={this.onOptionItemMouseDown(option)}>
            {option.text}
            <Ink opacit={0.1} />
          </li>
        )}
      </ul>
    );
  }

  get placeholder () {
    const { value, placeholder } = this.props;
    return value.value.text ? value.value.text : placeholder;
  }

  get error () {
    const { value } = this.props;

    return value && value.error ? (
      <div className='select__error'>
        {value.error}
      </div>
    ) : null;
  }

  componentDidMount () {
    const { initialValue } = this.props;
    if (!initialValue) {
      return;
    }

    this.changeValue(initialValue);
  }

  render () {
    return (
      <div className='select'>
        <i className='select__icon material-icons'>
          arrow_drop_down
        </i>
        <div
          onClick={this.onPlaceholderClick}
          className='select__placeholder'>
          {this.placeholder}
        </div>
        {this.error}
        <ReactCSSTransitionGroup
          transitionName='select__transition-group'
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {this.options}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}
