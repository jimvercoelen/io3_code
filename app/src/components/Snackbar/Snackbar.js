import React, { Component } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import './Snackbar.scss';

@connect(state => ({
  snackbar: state.ui.snackbar
}))
export default class Snackbar extends Component {
  render () {
    const { snackbar } = this.props;

    return (
      <div className='snackbar'>
        <div
          className={cx({
            'snackbar__inner': true,
            'show': snackbar.show
          })}>
          {snackbar.notification}
        </div>
      </div>
    );
  }
};
