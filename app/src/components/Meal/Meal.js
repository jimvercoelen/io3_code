import React, { PropTypes } from 'react';
import './Meal.scss';

const formatSubtitle = meal => {
  return 'E ' + meal.kcal + 'kcal / P ' + meal.protein + 'g' + '/ C ' + meal.carbohydrates + 'g / F ' + meal.fat + 'g';
};

const Meal = ({ meal, onPreviousClick, onNextClick, onMealClick }) => (
  <div
    className='meal'
    onClick={onMealClick}
    style={{
      backgroundImage: 'url(' + meal.photoUrl + ' )'
    }}>
    <i
      className='meal__icon meal__icon--left material-icons'
      onClick={onPreviousClick}>
      keyboard_arrow_left
    </i>
    <i
      className='meal__icon meal__icon--right material-icons'
      onClick={onNextClick}>
      keyboard_arrow_right
    </i>
    <div className='meal__details'>
      <div className='meal__title'>
        {meal.name}
      </div>
      <div className='meal__subtitle'>
        {formatSubtitle(meal)}
      </div>
    </div>
  </div>
);

Meal.propsTypes = {
  meal: PropTypes.object.isRequired,
  onPreviousClick: PropTypes.func.isRequired,
  onNextClick: PropTypes.func.isRequired,
  onMealClick: PropTypes.func.isRequired
};

export default Meal;
