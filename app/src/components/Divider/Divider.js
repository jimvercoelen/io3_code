import React, { PropTypes } from 'react';
import cx from 'classnames';
import './Divider.scss';

const getText = text => {
  return text ? (
    <div className='divider__text'>
      {text}
    </div>
  ) : null;
};

const Divider = ({ text }) => {
  const type = text ? 'with-text' : 'without-text';

  return (
    <div
      className={cx({
        'divider': true,
        [`divider--type-${type}`]: type
      })}>
      {getText(text)}
    </div>
  );
};

Divider.propTypes = {
  text: PropTypes.string,
  type: PropTypes.oneOf([
    'with-text',
    'without-text'
  ])
};

Divider.defaultProps = {
  type: 'without-text'
};

export default Divider;
