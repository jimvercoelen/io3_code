import React, { Component } from 'react';
import ReactSlider from 'react-slider';
import './Range.scss';

export default class Range extends Component {
  static propTypes = {
    // TODO
  };

  state = {
    value: this.props.defaultValue
  };

  onChange = value => {
    const { onChange, name } = this.props;

    this.setState({ value }, onChange(name, value));
  };

  get labelText () {
    const { label, labelEnd } = this.props;
    const { value } = this.state;
    const labelText = label + ' ' + value + ' ' + (labelEnd ? labelEnd : '');

    return labelText;
  }

  componentDidMount () {
    this.onChange(this.props.defaultValue);
  }

  render () {
    const { min, max, defaultValue } = this.props;

    return (
      <div className='range'>
        <div className='range__label'>
          {this.labelText}
        </div>
        <ReactSlider
          className='range__slider'
          min={min}
          max={max}
          defaultValue={defaultValue}
          onChange={this.onChange}
          barClassName='range__slider__trackbar'
          withBars>
          <div className='range__slider__thumb' />
        </ReactSlider>
      </div>
    );
  }
}
