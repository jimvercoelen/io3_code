import user from './user';
import meal from './meals';
import ui from './ui';

export default {
  user,
  meal,
  ui
};
