import createReducer from '../helpers/createReducer';

import {
  UI_SHOW_SNACKBAR,
  UI_HIDE_SNACKBAR,
  UI_SHOW_MODAL,
  UI_HIDE_MODAL
} from '../constants/ActionTypes';

const initialState = {
  snackbar: {
    show: false,
    notification: null
  },
  modal: {
    show: false,
    data: null
  }
};

export default createReducer(initialState, {
  [UI_SHOW_SNACKBAR]: (state, action) => ({
    snackbar: {
      show: true,
      notification: action.payload.notification
    }
  }),
  [UI_HIDE_SNACKBAR]: () => ({
    snackbar: initialState.snackbar
  }),

  [UI_SHOW_MODAL]: (state, action) => ({
    modal: {
      show: true,
      data: action.payload
    }
  }),
  [UI_HIDE_MODAL]: () => ({
    modal: initialState.modal
  })
});
