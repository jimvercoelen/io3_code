import createReducer from '../helpers/createReducer';
import {
  USER_LOGIN,
  USER_CREATE_ACCOUNT,
  USER_UPDATE,
  USER_LOGOUT,
  USER_LOAD
} from '../constants/ActionTypes';

const initialState = {
  auth: {},
  load: {
    pending: false,
    error: false
  },
  login: {
    pending: false,
    error: false
  },
  createAccount: {
    pending: false,
    error: false
  },
  updateProfile: {
    pending: false,
    error: false
  }
};

export default createReducer(initialState, {
  [USER_LOGIN + '_PENDING']: () => ({
    auth: initialState.auth,
    login: {
      pending: true,
      error: false
    }
  }),
  [USER_LOGIN + '_SUCCESS']: (state, action) => ({
    auth: action.payload,
    login: {
      pending: false
    }
  }),
  [USER_LOGIN + '_ERROR']: (state, action) => ({
    login: {
      pending: false,
      error: action.error
    }
  }),

  [USER_LOAD + '_PENDING']: () => ({
    auth: initialState.auth,
    load: {
      pending: true,
      error: false
    }
  }),
  [USER_LOAD + '_SUCCESS']: (state, action) => ({
    auth: action.payload,
    load: {
      pending: false
    }
  }),
  [USER_LOAD + '_ERROR']: (state, action) => ({
    load: {
      pending: false,
      error: action.error
    }
  }),

  [USER_CREATE_ACCOUNT + '_PENDING']: () => ({
    createAccount: {
      pending: true,
      error: false
    }
  }),
  [USER_CREATE_ACCOUNT + '_SUCCESS']: (state, action) => ({
    auth: action.payload,
    createAccount: {
      pending: false
    }
  }),
  [USER_CREATE_ACCOUNT + '_ERROR']: (state, action) => ({
    createAccount: {
      pending: false,
      error: action.error
    }
  }),

  [USER_UPDATE + '_PENDING']: () => ({
    updateProfile: {
      pending: true,
      error: false
    }
  }),
  [USER_UPDATE + '_SUCCESS']: (state, action) => ({
    auth: action.payload,
    updateProfile: {
      pending: false
    }
  }),
  [USER_UPDATE + '_ERROR']: (state, action) => ({
    updateProfile: {
      pending: false,
      error: action.error
    }
  }),

  [USER_LOGOUT]: () => ({
    auth: initialState.auth
  })
});
