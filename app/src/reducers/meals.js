import createReducer from '../helpers/createReducer';
import { parseMeals, parseNextMeals } from '../helpers/meal';

import {
  MEALS_LOAD,
  MEALS_LOAD_NEXT,
  MEAL_LOAD_INGREDIENTS
} from '../constants/ActionTypes';

const initialState = {
  meals: null,
  ingredients: null,
  load: {
    pending: false,
    error: false
  },
  loadNext: {
    pending: false,
    error: false
  },
  loadIngredients: {
    pending: false,
    error: false
  },
};

export default createReducer(initialState, {
  [MEALS_LOAD + '_PENDING']: () => ({
    meals: null,
    load: initialState.load
  }),
  [MEALS_LOAD + '_SUCCESS']: (state, action) => ({
    meals: parseMeals(action.payload),
    load: {
      pending: false
    }
  }),
  [MEALS_LOAD + '_ERROR']: (state, action) => ({
    load: {
      pending: false,
      error: action.error
    }
  }),

  [MEALS_LOAD_NEXT + '_PENDING']: () => ({
    nextMeals: null,
    loadNext: initialState.loadNext
  }),
  [MEALS_LOAD_NEXT + '_SUCCESS']: (state, action) => ({
    meals: parseNextMeals(state, action.payload),
    loadNext: {
      pending: false
    }
  }),
  [MEALS_LOAD_NEXT + '_ERROR']: (state, action) => ({
    loadNext: {
      pending: false,
      error: action.error
    }
  }),

  [MEAL_LOAD_INGREDIENTS + '_PENDING']: () => ({
    ingredients: null,
    loadIngredients: initialState.loadIngredients
  }),
  [MEAL_LOAD_INGREDIENTS + '_SUCCESS']: (state, action) => ({
    ingredients: action.payload,
    loadIngredients: {
      pending: false
    }
  }),
  [MEAL_LOAD_INGREDIENTS + '_ERROR']: (state, action) => ({
    loadIngredients: {
      pending: false,
      error: action.error
    }
  })
});
