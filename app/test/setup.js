import React from 'react';
import { createRenderer, renderIntoDocument, scryRenderedDOMComponentsWithClass } from 'react-addons-test-utils';
import expect from 'expect';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

global.React = React;
global.createRenderer = createRenderer;
global.renderIntoDocument = renderIntoDocument;
global.scryRenderedDOMComponentsWithClass = scryRenderedDOMComponentsWithClass;
global.expect = expect;
global.expectJSX = expectJSX;
