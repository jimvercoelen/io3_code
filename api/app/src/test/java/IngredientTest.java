import io.vertx.core.Vertx;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.TestSuite;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import models.Ingredient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import verticles.SingleAppVerticle;

/**
 * Created by Walter on 22/12/2016.
 */
public class IngredientTest {
  Ingredient i;

  @Before
  public void setUp() {
    i = new Ingredient(14, "test", 0.0, 9.0, "test", "test", 0.0, 93.0, 0.0, 22.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 23.0, 10.0, 95.0, 0.0, 1.0, 25.0, 9.0, 0.0, 2.0, 0.0, 1.0, 224.0, 47.0, 1.0);
  }

  @After
  public void tearDown() {
    //TODO
  }

  @Test
  public void testConstructor() {
    TestSuite suite = TestSuite.create("the_test_suite");
    suite.test("testConstructor", context -> {
      Ingredient expected = new Ingredient(14, "test", 0.0, 9.0, "test", "test", 0.0, 93.0, 0.0, 22.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 23.0, 10.0, 95.0, 0.0, 1.0, 25.0, 9.0, 0.0, 2.0, 0.0, 1.0, 224.0, 47.0, 1.0);
      context.assertEquals(i, expected);
    });
    suite.run();
  }
}
