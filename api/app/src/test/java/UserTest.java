import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;

import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.TestSuite;
import models.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Walter on 22/12/2016.
 */
public class UserTest {
  User u;

  @Before
  public void setUp() {
    u = new User(1, "test.test@test.nl", "Afvallen", "m", 5.0, "Melk", 23, 85, 183, 4, 2400);
  }

  @After
  public void tearDown() {
    //TODO
  }

  @Test
  public void testConstructor() {
    TestSuite suite = TestSuite.create("the_test_suite");
    suite.test("testConstructor", context -> {
      User expected = new User(1, "test.test@test.nl", "Afvallen", "m", 5.0, "Melk", 23, 85, 183, 4, 2400);
      context.assertEquals(u, expected);
    });
    suite.run();
  }
}
