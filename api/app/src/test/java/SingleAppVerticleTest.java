import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.TestSuite;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.RoutingContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import verticles.SingleAppVerticle;

@RunWith(VertxUnitRunner.class)
public class SingleAppVerticleTest {
  private Vertx vertx;
  private static final Logger LOGGER = LoggerFactory.getLogger(SingleAppVerticle.class);

  @Before
  public void setUp(TestContext context) {
    vertx = Vertx.vertx();
    vertx.deployVerticle(SingleAppVerticle.class.getName(),
      context.asyncAssertSuccess());
  }

  @After
  public void tearDown(TestContext context) {
    vertx.close(context.asyncAssertSuccess());
  }

  /*@Test
  public void testMyApplication(TestContext context) {
    final Async async = context.async();

    vertx
      .createHttpClient()
      .getNow(8080, "localhost", "/", response -> {
        response.handler(body -> {
          context.assertTrue(body.toString().contains("Hello"));
          async.complete();
        });
      });
  }*/

  //If this test fails the unit tests dont work.
  @Test
  public void testUnitTest() {
    TestSuite suite = TestSuite.create("the_test_suite");
    suite.test("my_test_case", context -> {
      String s = "value";
      context.assertEquals("value", s);
    });
    suite.run();
  }

  private void handleException(Exception e, RoutingContext context) {
    badRequest(context);

    LOGGER.error("Exception: ", e.getMessage());
    e.printStackTrace();
  }

  private void badRequest(RoutingContext context) {
    context.response().setStatusCode(400).end();
  }
}
