import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.TestSuite;
import models.Meal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Walter on 22/12/2016.
 */
public class MealTest {
  Meal m;

  @Before
  public void setUp() {
    m = new Meal(1,"TestMeal","breakfast","gwn eten gek.", "met bestek is handig.", "https://storage.demediahub.nl/15400000/15350000/15348000/144680572057295_15347721_1920.jpg", 1000, 340, 15, 30, 20);
  }

  @After
  public void tearDown() {
    //TODO
  }

  @Test
  public void testConstructor() {
    TestSuite suite = TestSuite.create("the_test_suite");
    suite.test("testConstructor", context -> {
      Meal expected = new Meal(1,"TestMeal","breakfast","gwn eten gek.", "met bestek is handig.", "https://storage.demediahub.nl/15400000/15350000/15348000/144680572057295_15347721_1920.jpg", 1000, 340, 15, 30, 20);
      context.assertEquals(m, expected);
    });
    suite.run();
  }
}
