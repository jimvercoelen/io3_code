package verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import models.User;
import services.JdbcMealService;
import services.JdbcUserService;
import services.MealService;
import services.UserService;
import util.Constants;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class SingleAppVerticle extends AbstractVerticle {

  private static final Logger LOGGER = LoggerFactory.getLogger(SingleAppVerticle.class);

  private Router router;
  private JDBCClient client;
  private UserService userService;
  private MealService mealService;

  @Override
  public void start(Future<Void> future) throws Exception {

    initJdbcClient();
    initServices();
    initRouter();
    initRoutes();

    vertx.createHttpServer()
      .requestHandler(router::accept)
      .listen(
        config().getInteger("http.port", 8080),
        result -> {
          if (result.succeeded())
            future.complete();
          else
            future.fail(result.cause());
        });
  }

  private void initJdbcClient() {
    JsonObject clientConfig = new JsonObject()
      .put("userService.type", "jdbc")
      .put("url", "jdbc:mysql://localhost:3306/shaping?characterEncoding=UTF-8&useSSL=false")
      .put("driver_class", "com.mysql.cj.jdbc.Driver")
      .put("user", "root")
      .put("password", "root")
      .put("max_pool_size", 30);

    client = JDBCClient.createShared(vertx, clientConfig);
  }

  private void initServices() {
    userService = new JdbcUserService(client);
    mealService = new JdbcMealService(client);
  }

  private void initRouter() {
    // Cors support
    Set<String> allowHeaders = new HashSet<>();
    allowHeaders.add("x-requested-with");
    allowHeaders.add("Access-Control-Allow-Origin");
    allowHeaders.add("origin");
    allowHeaders.add("Content-Type");
    allowHeaders.add("accept");

    Set<HttpMethod> allowMethods = new HashSet<>();
    allowMethods.add(HttpMethod.GET);
    allowMethods.add(HttpMethod.POST);
    allowMethods.add(HttpMethod.PUT);
    allowMethods.add(HttpMethod.DELETE);
    allowMethods.add(HttpMethod.PATCH);

    CorsHandler corsHandler = CorsHandler.create("*")
      .allowedHeaders(allowHeaders)
      .allowedMethods(allowMethods);

    router = Router.router(vertx);
    router.route().handler(corsHandler);
    router.route().handler(BodyHandler.create());
  }

  private void initRoutes() {
    // User routes
    router.post(Constants.GET_AUTH).handler(this::handleGetAuth);
    router.get(Constants.GET_USER).handler(this::handleGetUser);
    router.post(Constants.CREATE_USER).handler(this::handleCreateUser);
    router.patch(Constants.UPDATE_USER).handler(this::handleUpdateUser);
    router.delete(Constants.DELETE_USER).handler(this::handleDeleteUser);

    // Meal routes
    router.get(Constants.GET_MEALS).handler(this::handleGetMeals);
    router.get(Constants.GET_NEXT_MEALS).handler(this::handleGetNextMeals);
    router.get(Constants.GET_MEAL_INGREDIENTS).handler(this::handleGetMealIngredients);

  }

  /**
   * Wrapped Result handler with failure handler
   */
  private <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context, Consumer<T> consumer) {
    return result -> {
      if (result.succeeded()) {
        consumer.accept(result.result());
      } else {
        // send response depending on the error
        if (result.cause().getMessage().contains("Duplicate entry")) {
          notFound(context);
        } else {
          badRequest(context);
        }

        LOGGER.error("Result failed: ", result.cause().getMessage());
        result.cause().printStackTrace();
      }
    };
  }

  private void handleGetAuth(RoutingContext context) {
    if (hasEmptyBody(context)) {
      badRequest(context);
      return;
    }

    try {
      final User user = Json.decodeValue(context.getBodyAsString(), User.class);

      userService.getAuth(user.getEmail(), user.getPassword()).setHandler(resultHandler(context, result -> {
        if (result.isPresent()) {
          final String encoded = Json.encodePrettily(result.get());
          context.response().end(encoded);
        } else {
          notFound(context);
        }
      }));
    } catch (DecodeException e) {
      badRequest(context);
    }
  }

  private void handleGetUser(RoutingContext context) {
    int id;
    try {
      id = Integer.parseInt(context.request().getParam("id"));
    } catch (NumberFormatException e) {
      handleException(e, context);
      return;
    }

    try {
      userService.getUser(id).setHandler(resultHandler(context, result -> {
        if (result.isPresent()) {
          final String encoded = Json.encodePrettily(result.get());
          context.response().end(encoded);
        } else {
          notFound(context);
        }
      }));
    } catch (DecodeException e) {
      badRequest(context);
    }
  }

  private void handleCreateUser(RoutingContext context) {
    if (hasEmptyBody(context)) {
      badRequest(context);
      return;
    }

    try {
      User user = Json.decodeValue(context.getBodyAsString(), User.class);
      userService.insert(user).setHandler(resultHandler(context, result -> {
        if (result != null) {
          user.setId(result);
          final String encoded = Json.encodePrettily(user.withoutPassword());
          context.response().end(encoded);
        } else {
          notFound(context);
        }
      }));
    } catch (DecodeException e) {
      handleException(e, context);
    }
  }

  private void handleUpdateUser(RoutingContext context) {
    if (hasEmptyBody(context)) {
      badRequest(context);
      return;
    }

    try {
      final User updatedUser = User.parseBody(context.getBodyAsJson());
      final String encoded = Json.encode(updatedUser.withoutPassword());

      userService.update(updatedUser).setHandler(resultHandler(context, result -> {
        if (result != null) {
          context.response().end(encoded);
        } else {
          notFound(context);
        }
      }));
    } catch (DecodeException e) {
      handleException(e, context);
    }
  }

  private void handleDeleteUser(RoutingContext context) {
    int id;
    try {
      id = Integer.parseInt(context.request().getParam("id"));
    } catch (NumberFormatException e) {
      handleException(e, context);
      return;
    }

    userService.delete(id).setHandler(resultHandler(context, result -> {
      if (result) {
        context.response().end();
      } else {
        notFound(context);
      }
    }));
  }

  private void handleGetMeals(RoutingContext context) {
    int mealsADay;
    try {
      mealsADay = Integer.parseInt(context.request().getParam("mealsADay"));
    } catch (NumberFormatException e) {
      handleException(e, context);
      return;
    }
    mealService.getMeals(mealsADay).setHandler(resultHandler(context, result -> {
      if (result != null && result.size() != 0) {
        final String encoded = Json.encodePrettily(result);
        context.response().end(encoded);
      } else {
        notFound(context);
      }
    }));
  }

  private void handleGetNextMeals(RoutingContext context) {
    int lastId;
    int amount = 3;
    String time = context.request().getParam("time");
    if (time == null) {
      badRequest(context);
    }

    try {
      lastId = Integer.parseInt(context.request().getParam("lastId"));
    } catch (NumberFormatException e) {
      handleException(e, context);
      return;
    }

    mealService.getNextMeals(time, lastId, amount).setHandler(resultHandler(context, result -> {
      final String encoded = Json.encodePrettily(result);
      if (result != null && result.size() != 0) {
        context.response().end(encoded);
      } else {
        notFound(context);
      }
    }));
  }

  private void handleGetMealIngredients(RoutingContext context) {
    int mealId;
    try {
      mealId = Integer.parseInt(context.request().getParam("id"));
    } catch (NumberFormatException e) {
      handleException(e, context);
      return;
    }

    mealService.getMealIngredients(mealId).setHandler(resultHandler(context, result -> {
      if (result != null && result.size() != 0) {
        final String encoded = Json.encodePrettily(result);
        context.response().end(encoded);
      } else {
        notFound(context);
      }
    }));
  }

  private void handleException(Exception e, RoutingContext context) {
    badRequest(context);

    LOGGER.error("Exception: ", e.getMessage());
    e.printStackTrace();
  }

  private boolean hasEmptyBody(RoutingContext context) {
    return context.getBodyAsString() == null || context.getBodyAsString().isEmpty();
  }

  private void sendError(int statusCode, HttpServerResponse response) {
    response.setStatusCode(statusCode).end();
  }

  private void badRequest(RoutingContext context) {
    context.response().setStatusCode(400).end();
  }

  private void notFound(RoutingContext context) {
    context.response().setStatusCode(404).end();
  }

  private void serviceUnavailable(RoutingContext context) {
    context.response().setStatusCode(503).end();
  }
}
