package services;

import io.vertx.core.Future;
import models.Ingredient;
import models.Meal;
import java.util.List;

public interface MealService {

  Future<List<Meal>> getMeals(int mealsADay);

  Future<List<Meal>> getNextMeals(String sort, int lastId, int amount);

  Future<List<Ingredient>> getMealIngredients(int id);
}
