package services;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import models.Ingredient;
import models.Meal;

import java.util.List;
import java.util.stream.Collectors;

public class JdbcMealService implements MealService  {

  private final JDBCClient client;

  public JdbcMealService(JDBCClient client) {
    this.client = client;
  }

  private static final String SQL_PROCEDURE_GET_MEALS = "CALL getMeals(?, ?, ?, ?, ?, ?, ?, ?)";
  private static final String SQL_PROCEDURE_GET_NEXT_MEALS = "CALL getNextMeals(?, ?, ?)";
  private static final String SQL_PROCEDURE_GET_MEAL_INGREDIENTS = "CALL getMealIngredients(?)";

  private Handler<AsyncResult<SQLConnection>> connectionHandler(Future future, Handler<SQLConnection> handler) {
    return conn -> {
      if (conn.succeeded()) {
        final SQLConnection connection = conn.result();
        handler.handle(connection);
      } else {
        future.fail(conn.cause());
      }
    };
  }


  @Override
  public Future<List<Meal>> getMeals(int mealsADay) {

    Future<List<Meal>> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.queryWithParams(
        SQL_PROCEDURE_GET_MEALS,
        getMealsParameters(mealsADay),
        r -> {
        if (r.succeeded()) {
          List<Meal> meals = r.result().getRows().stream().map(Meal::new).collect(Collectors.toList());
          result.complete(meals);
        } else {
          result.fail(r.cause());
        }

        connection.close();
      });
    }));

    return result;
  }

  @Override
  public Future<List<Meal>> getNextMeals(String time, int lastId, int amount) {
    Future<List<Meal>> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.queryWithParams(
        SQL_PROCEDURE_GET_NEXT_MEALS,
        new JsonArray().add(time).add(lastId).add(amount),
        r -> {
        if (r.succeeded()) {
          List<Meal> meals = r.result().getRows().stream().map(Meal::new).collect(Collectors.toList());
          result.complete(meals);
        } else {
          result.fail(r.cause());
        }

        connection.close();
      });
    }));

    return result;
  }

  @Override
  public Future<List<Ingredient>> getMealIngredients(int id) {
    Future<List<Ingredient>> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.queryWithParams(
        SQL_PROCEDURE_GET_MEAL_INGREDIENTS,
        new JsonArray().add(id),
        r -> {
          if (r.succeeded()) {
            List<Ingredient> ingredients = r.result().getRows().stream().map(Ingredient::new).collect(Collectors.toList());
            result.complete(ingredients);
          } else {
            result.fail(r.cause());
          }

          connection.close();
        });
    }));

    return result;
  }

  /** Returns a JsonArray containing meal times corresponding with the mealsADay */
  private JsonArray getMealsParameters(int mealsADay) {
    JsonArray mealTimes = new JsonArray();
    switch (mealsADay) {
      case 3:
        mealTimes.add("09.00");
        mealTimes.add("12.00");
        mealTimes.add("17.30");
        mealTimes.add("");
        mealTimes.add("");
        mealTimes.add("");
        mealTimes.add("");
        mealTimes.add("");
        break;
      case 4:
        mealTimes.add("09.00");
        mealTimes.add("12.00");
        mealTimes.add("17.30");
        mealTimes.add("19.30");
        mealTimes.add("");
        mealTimes.add("");
        mealTimes.add("");
        mealTimes.add("");
        break;
      case 5:
        mealTimes.add("09.00");
        mealTimes.add("12.00");
        mealTimes.add("15.00");
        mealTimes.add("17.30");
        mealTimes.add("19.30");
        mealTimes.add("");
        mealTimes.add("");
        mealTimes.add("");
        break;
      case 6:
        mealTimes.add("09.00");
        mealTimes.add("12.00");
        mealTimes.add("15.00");
        mealTimes.add("17.30");
        mealTimes.add("19.30");
        mealTimes.add("22.00");
        mealTimes.add("");
        mealTimes.add("");
        break;
      case 7:
        mealTimes.add("09.00");
        mealTimes.add("12.00");
        mealTimes.add("15.00");
        mealTimes.add("17.30");
        mealTimes.add("19.00");
        mealTimes.add("19.30");
        mealTimes.add("22.00");
        mealTimes.add("");
        break;
      case 8:
        mealTimes.add("06.30");
        mealTimes.add("09.00");
        mealTimes.add("12.00");
        mealTimes.add("15.00");
        mealTimes.add("17.30");
        mealTimes.add("19.00");
        mealTimes.add("19.30");
        mealTimes.add("22.00");
        break;
    }

    return mealTimes;
  }
}
