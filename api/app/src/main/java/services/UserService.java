package services;

import io.vertx.core.Future;
import models.User;

import java.util.Optional;

public interface UserService {

  Future<Integer> insert(User newUser);

  Future<Optional<User>> getAuth(String email, String password);

  Future<Optional<User>> getUser(int id);

  Future<Boolean> update(User updatedUser);

  Future<Boolean> delete(int id);
}
