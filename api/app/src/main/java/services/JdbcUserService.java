package services;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import models.User;

import java.util.List;
import java.util.Optional;

public class JdbcUserService implements UserService {

  private final JDBCClient client;

  public JdbcUserService(JDBCClient client) {
    this.client = client;
  }

  private static final String SQL_INSERT = "INSERT INTO `user` VALUES (null, ?, ?, null, null, null, null, null, null, null, null, null)";
  private static final String SQL_QUERY = "SELECT * FROM `user` WHERE `id` = ?";
  private static final String SQL_QUERY_AUTH = "SELECT * FROM `user` WHERE `email` = ? AND `password` = ?";
  private static final String SQL_UPDATE = "UPDATE `user` SET `goal` = ?, `gender` = ?, `activity` = ?, `allergies` = ?, `age` = ?, `weight` = ?, `height` = ?, `meals_a_day` = ?, `need` = ? WHERE `id` = ?";
  private static final String SQL_DELETE = "DELETE FROM `user` WHERE `id` = ?";

  private Handler<AsyncResult<SQLConnection>> connectionHandler(Future future, Handler<SQLConnection> handler) {
    return conn -> {
      if (conn.succeeded()) {
        final SQLConnection connection = conn.result();
        handler.handle(connection);
      } else {
        future.fail(conn.cause());
      }
    };
  }

  @Override
  public Future<Integer> insert(User newUser) {
    Future<Integer> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.updateWithParams(
        SQL_INSERT, new JsonArray().add(newUser.getEmail()).add(newUser.getPassword()), r -> {
          if (r.succeeded()) {
            result.complete(r.result().getKeys().getInteger(0));
          } else {
            result.fail(r.cause());
          }

          connection.close();
      });
    }));

    return result;
  }

  @Override
  public Future<Optional<User>> getAuth(String email, String password) {
    Future<Optional<User>> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.queryWithParams(SQL_QUERY_AUTH, new JsonArray().add(email).add(password), r -> {
        if (r.succeeded()) {
          List<JsonObject> list = r.result().getRows();
          if (list == null || list.isEmpty()) {
            result.complete(Optional.empty());
          } else {
            result.complete(Optional.of(new User(list.get(0))));
          }
        } else {
          result.fail(r.cause());
        }

        connection.close();
      });
    }));

    return result;
  }

  @Override
  public Future<Optional<User>> getUser(int id) {
    Future<Optional<User>> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.queryWithParams(SQL_QUERY, new JsonArray().add(id), r -> {
        if (r.succeeded()) {
          List<JsonObject> list = r.result().getRows();
          if (list == null || list.isEmpty()) {
            result.complete(Optional.empty());
          } else {
            result.complete(Optional.of(new User(list.get(0))));
          }
        } else {
          result.fail(r.cause());
        }

        connection.close();
      });
    }));

    return result;
  }

  @Override
  public Future<Boolean> update(User updatedUser) {
    Future<Boolean> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.updateWithParams(
        SQL_UPDATE,
        new JsonArray().add(updatedUser.getGoal())
          .add(updatedUser.getGender())
          .add(updatedUser.getActivity())
          .add(updatedUser.getAllergies())
          .add(updatedUser.getAge())
          .add(updatedUser.getWeight())
          .add(updatedUser.getHeight())
          .add(updatedUser.getMealsADay())
          .add(updatedUser.getNeed())
          .add(updatedUser.getId()),
        r -> {
          if (r.succeeded()) {
            result.complete(true);
          } else {
            result.fail(r.cause());
          }

          connection.close();
        });
    }));

    return result;
  }

  @Override
  public Future<Boolean> delete(int id) {
    Future<Boolean> result = Future.future();
    client.getConnection(connectionHandler(result, connection -> {
      connection.updateWithParams(SQL_DELETE, new JsonArray().add(id), r -> {
        if (r.succeeded()) {
          result.complete(true);
        } else {
          result.fail(r.cause());
        }

        connection.close();
      });
    }));

    return result;
  }
}
