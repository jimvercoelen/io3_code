package util;

public final class Constants {

  private Constants() {}

  /** API ROUTES */
  public static final String GET_AUTH = "/auth";
  public static final String GET_USER = "/user/:id";
  public static final String CREATE_USER = "/user";
  public static final String UPDATE_USER = "/user";
  public static final String DELETE_USER = "/user/:id";

  /** Meal routes */
  public static final String GET_MEALS = "/meals/:mealsADay";
  public static final String GET_NEXT_MEALS = "/meals/:time/:lastId";
  public static final String GET_MEAL_INGREDIENTS = "/meal/ingredients/:id";
}
