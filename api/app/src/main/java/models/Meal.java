package models;

import io.vertx.core.json.JsonObject;

public class Meal {
  private final int id;
  private final String name;
  private final String time;
  private final String description;
  private final String tip;
  private final String photoUrl;
  private final double kj;
  private final double kcal;
  private final double protein;
  private final double carbohydrates;
  private final double fat;

  public Meal(
    int id,
    String name,
    String sort,
    String description,
    String tip,
    String photoUrl,
    double kj,
    double kcal,
    double protein,
    double carbohydrates,
    double fat) {
    this.id = id;
    this.name = name;
    this.time = sort;
    this.description = description;
    this.tip = tip;
    this.photoUrl = photoUrl;
    this.kj = kj;
    this.kcal = kcal;
    this.protein = protein;
    this.carbohydrates = carbohydrates;
    this.fat = fat;
  }

  public Meal(JsonObject entries) {
    this.id = entries.getInteger("id");
    this.name = entries.getString("name");
    this.time = entries.getString("time");
    this.description = entries.getString("description");
    this.tip = entries.getString("tip");
    this.photoUrl = entries.getString("photo_url");
    this.kj = entries.getDouble("kj");
    this.kcal = entries.getDouble("kcal");
    this.protein = entries.getDouble("protein");
    this.carbohydrates = entries.getDouble("carbohydrates");
    this.fat = entries.getDouble("fat");
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getTime() {
    return time;
  }

  public String getDescription() {
    return description;
  }

  public String getTip() {
    return tip;
  }

  public String getPhotoUrl() {
    return photoUrl;
  }

  public double getKj() {
    return kj;
  }

  public double getKcal() {
    return kcal;
  }

  public double getProtein() {
    return protein;
  }

  public double getCarbohydrates() {
    return carbohydrates;
  }

  public double getFat() {
    return fat;
  }
}
