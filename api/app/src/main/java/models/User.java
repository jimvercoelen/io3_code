package models;

import io.vertx.core.json.JsonObject;

public class User {

  private int id;
  private String email;
  private String password;
  private String goal;
  private String gender;
  private Double activity;
  private String allergies;
  private Integer age;
  private Integer weight;
  private Integer height;
  private Integer mealsADay;
  private Integer need;

  public User() {}

  public User(JsonObject entries) {
    this.id = entries.getInteger("id");
    this.email = entries.getString("email");
    this.gender = entries.getString("gender");
    this.goal = entries.getString("goal");
    this.activity = entries.getDouble("activity");
    this.allergies = entries.getString("allergies");
    this.age = entries.getInteger("age");
    this.weight = entries.getInteger("weight");
    this.height = entries.getInteger("height");
    this.mealsADay = entries.getInteger("meals_a_day");
    this.need = entries.getInteger("need");
  }

  public User(int id, String email, String goal, String gender, Double activity, String allergies, Integer age, Integer weight, Integer height, Integer mealsADay, Integer need) {
    this.id = id;
    this.email = email;
    this.goal = goal;
    this.gender = gender;
    this.activity = activity;
    this.allergies = allergies;
    this.age = age;
    this.weight = weight;
    this.height = height;
    this.mealsADay = mealsADay;
    this.need = need;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public String getGoal() {
    return goal;
  }

  public String getGender() {
    return gender;
  }

  public Double getActivity() {
    return activity;
  }

  public String getAllergies() {
    return allergies;
  }

  public Integer getAge() {
    return age;
  }

  public Integer getWeight() {
    return weight;
  }

  public Integer getHeight() {
    return height;
  }

  public Integer getMealsADay() {
    return mealsADay;
  }

  public Integer getNeed() {
    return need;
  }

  public static User parseBody(JsonObject entries) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0, l = entries.getJsonArray("allergies").size(); i < l; i++) {
      stringBuilder
        .append(entries.getJsonArray("allergies").getString(i))
        .append(",");
    }

    return new User(
      entries.getInteger("id"),
      entries.getString("email"),
      entries.getString("goal"),
      entries.getString("gender"),
      entries.getDouble("activity"),
      stringBuilder.toString(),
      entries.getInteger("age"),
      entries.getInteger("weight"),
      entries.getInteger("height"),
      entries.getInteger("mealsADay"),
      entries.getInteger("need"));
  }

  public User withoutPassword() {
    return new User(id, email, goal, gender, activity, allergies, age, weight, height, mealsADay, need);
  }

  /*private <T> T getOrElse(T value, T defaultValue) {
    return value == null ? defaultValue : value;
  }*/

  @Override
  public String toString() {
    return email + " " + goal + " " + gender + " " + activity + " " + allergies + " " + age + " " + height + " " + weight + " " + mealsADay + " " + need;
  }

}
