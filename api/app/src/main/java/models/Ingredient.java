package models;

import io.vertx.core.json.JsonObject;

public class Ingredient {
  private int id;
  private String productGroup;
  private double productGroupCode;
  private double productCode;
  private String productOmschrijving;
  private String productDescription;
  private double kcal;
  private double kj;
  private double protein;
  private double fat;
  private double saturatedFats;
  private double monounsaturated;
  private double polyunsaturated;
  private double cholesterol;
  private double carbohydrates;
  private double sugars;
  private double fibers;
  private double water;
  private double calcium;
  private double phosphorus;
  private double iron;
  private double sodium;
  private double potassium;
  private double magnesium;
  private double zinc;
  private double copper;
  private double v_b1;
  private double v_b2;
  private double v_b6;
  private double v_b12;
  private double v_d;
  private double v_e;
  private double v_c;

  public Ingredient() {}

  public Ingredient(
    int id,
    String productGroep,
    double productGroepCode,
    double productCode,
    String productOmschrijving,
    String productDescription,
    double kcal,
    double kj,
    double protein,
    double fat,
    double saturatedFats,
    double monounsaturated,
    double polyunsaturated,
    double cholesterol,
    double carbohydrates,
    double sugars,
    double fibers,
    double water,
    double calcium,
    double phosphorus,
    double iron,
    double sodium,
    double potassium,
    double magnesium,
    double zinc,
    double copper,
    double v_b1,
    double v_b2,
    double v_b6,
    double v_b12,
    double v_d,
    double v_e,
    double v_c) {
    this.id = id;
    this.productGroup = productGroep;
    this.productGroupCode = productGroepCode;
    this.productCode = productCode;
    this.productOmschrijving = productOmschrijving;
    this.productDescription = productDescription;
    this.kcal = kcal;
    this.kj = kj;
    this.protein = protein;
    this.fat = fat;
    this.saturatedFats = saturatedFats;
    this.monounsaturated = monounsaturated;
    this.polyunsaturated = polyunsaturated;
    this.cholesterol = cholesterol;
    this.carbohydrates = carbohydrates;
    this.sugars = sugars;
    this.fibers = fibers;
    this.water = water;
    this.calcium = calcium;
    this.phosphorus = phosphorus;
    this.iron = iron;
    this.sodium = sodium;
    this.potassium = potassium;
    this.magnesium = magnesium;
    this.zinc = zinc;
    this.copper = copper;
    this.v_b1 = v_b1;
    this.v_b2 = v_b2;
    this.v_b6 = v_b6;
    this.v_b12 = v_b12;
    this.v_d = v_d;
    this.v_e = v_e;
    this.v_c = v_c;
  }

  public Ingredient(JsonObject entries) {
    this.id = entries.getInteger("id");
    this.calcium = entries.getInteger("calcium");
    this.cholesterol = entries.getInteger("cholesterol");
    this.monounsaturated = entries.getInteger("e_o_vetten");
    this.protein = entries.getInteger("eiwit");
    this.kcal = entries.getInteger("kcal");
    this.kj = entries.getInteger("kj");
    this.phosphorus = entries.getInteger("fosfor");
    this.iron = entries.getInteger("ijzer");
    this.potassium = entries.getInteger("kalium");
    this.carbohydrates = entries.getInteger("koolhydraten");
    this.copper = entries.getInteger("koper");
    this.polyunsaturated = entries.getInteger("m_o_vetten");
    this.magnesium = entries.getInteger("magnesium");
    this.sodium = entries.getInteger("natrium");
    this.productCode = entries.getInteger("product_code");
    this.productGroup = entries.getString("product_groep");
    this.productGroupCode = entries.getInteger("product_groep_code");
    this.productDescription = entries.getString("product_description");
    this.productOmschrijving = entries.getString("product_omschrijving");
    this.sugars = entries.getInteger("suikers");
    this.v_b1 = entries.getInteger("v_b1");
    this.v_b2 = entries.getInteger("v_b2");
    this.v_b6 = entries.getInteger("v_b6");
    this.v_b12 = entries.getInteger("v_b12");
    this.v_c = entries.getInteger("v_c");
    this.v_d = entries.getInteger("v_d");
    this.v_e = entries.getInteger("v_e");
    this.saturatedFats = entries.getInteger("verzadige_vetten");
    this.fat = entries.getInteger("vet");
    this.fibers = entries.getInteger("vezels");
    this.water = entries.getInteger("water");
    this.zinc = entries.getInteger("zink");
  }

  public int getId() {
    return id;
  }

  public String getProductGroup() {
    return productGroup;
  }

  public double getProductGroupCode() {
    return productGroupCode;
  }

  public double getProductCode() {
    return productCode;
  }

  public String getProductOmschrijving() {
    return productOmschrijving;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public double getKcal() {
    return kcal;
  }

  public double getKj() {
    return kj;
  }

  public double getProtein() {
    return protein;
  }

  public double getFat() {
    return fat;
  }

  public double getSaturatedFats() {
    return saturatedFats;
  }

  public double getMonounsaturated() {
    return monounsaturated;
  }

  public double getPolyunsaturated() {
    return polyunsaturated;
  }

  public double getCholesterol() {
    return cholesterol;
  }

  public double getCarbohydrates() {
    return carbohydrates;
  }

  public double getSugars() {
    return sugars;
  }

  public double getFibers() {
    return fibers;
  }

  public double getWater() {
    return water;
  }

  public double getCalcium() {
    return calcium;
  }

  public double getPhosphorus() {
    return phosphorus;
  }

  public double getIron() {
    return iron;
  }

  public double getSodium() {
    return sodium;
  }

  public double getPotassium() {
    return potassium;
  }

  public double getMagnesium() {
    return magnesium;
  }

  public double getZinc() {
    return zinc;
  }

  public double getCopper() {
    return copper;
  }

  public double getV_b1() {
    return v_b1;
  }

  public double getV_b2() {
    return v_b2;
  }

  public double getV_b6() {
    return v_b6;
  }

  public double getV_b12() {
    return v_b12;
  }

  public double getV_d() {
    return v_d;
  }

  public double getV_e() {
    return v_e;
  }

  public double getV_c() {
    return v_c;
  }
}
